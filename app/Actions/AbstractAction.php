<?php
namespace App\Actions;

use Webpatser\Uuid\Uuid;
use Illuminate\Bus\Queueable;
use App\Actions\ActionPayload;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Imtigger\LaravelJobStatus\Trackable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class AbstractAction implements ActionInterface, ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, Trackable;

	/**
	 * @var ActionPayload
	 */
    public $payload;

    /**
     * @var string
     */
    public $uuid;

	/**
	 * @var mixed[]
	 */
	protected $dispatches = [];

	/**
	 * Create a new instance of a sync action
	 * @param ActionPayload $payload
	 */
	public function __construct(ActionPayload $payload) {
        $this->payload = $payload;
        $this->uuid = $this->tag();
        $this->prepareStatus(['key' => $this->tag()]);
	}

	/**
	 * Dispatch events from a list of events after the action has been handled.
     *
	 * @return void
	 */
	public function handle() {
		foreach ($this->dispatches as $dispatchable){
			event(new $dispatchable($this));
        }

        return true;
    }

    /**
     * Get the tags that should be assigned to the job.
     *
     * @return array
     */
    public function tags() {
        return ['action', $this->tag()];
    }

    /**
     * Return the tag for this action
     * if the input_id isn't available, generate one.
     *
     * @return string
     */
    public function tag(){
        return 'action:' . $this->payload->input_id ?? Uuid::generate();
    }

}
