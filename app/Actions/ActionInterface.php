<?php
namespace App\Actions;


/**
 * Interface ActionInterface
 *
 * @package App\Actions
 */
interface ActionInterface
{

    /**
     * Action Handler.
     * @return bool
     */
    public function handle();
}
