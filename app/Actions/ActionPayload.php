<?php
namespace App\Actions;

class ActionPayload
{
	/**
	 * @var mixed|null
	 */
	public $resource;

	/**
	 * ActionPayload constructor.
	 * @param mixed|null $resource
	 */
	public function __construct(
		$resource
	) {
		$this->resource = $resource;
	}

	public function __get($name) {
		if (!property_exists($this, $name)){
			return $this->resource->$name ?? null;
        }

        return $this->$name;
	}
}
