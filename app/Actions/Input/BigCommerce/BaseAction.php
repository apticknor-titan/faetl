<?php
namespace App\Actions\Input\BigCommerce;

use App\Actions\AbstractAction;
use App\Context;

class BaseAction extends AbstractAction
{

    /**
     * This is the base BigCommerce Action.
     * This action handles all BigCommerce Webhooks
     *
     * @return void
     */
	public function handle() {

        // Call Handled Events
		parent::handle();
    }


}
