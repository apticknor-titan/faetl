<?php
namespace App\Actions\Input\BigCommerce;

use App\Actions\Input\BigCommerce\BaseAction;
use App\Context;

class OrderCreatedAction extends BaseAction
{
    /**
     * This action handles store/orders/created
     *
     * When this event is dispatched it is in response to an LCE (Lightweight Callback Event)
     * The LCE payload has an entity type and an ID
     *
     * The goal here is to make an API call to the entity type endpoint and retrieve the record,
     * map it and then dispatch out an outbound solution.
     *
     * @return void
     */
	public function handle() {
        // 0 - Bootstrap the Project
        $context = resolve('context');

        $record = $this->payload->resource;

        /** @var App\Solutions\SolutionInterface $solution */
        if (!$project = $context->project($record->project)->load()){
            throw new FailedToLoadProjectException();
        }

        // Project is booted
        $project->boot();

        // Run the solution with record data.
        $project->run($record->solution, $record);

        // Call Handled Events
		return parent::handle();
    }
}
