<?php
namespace App\Actions\Input;

use App\Actions\AbstractAction;
use App\Actions\ActionPayload;
use App\Context;
use App\Models\Record;
use App\Exceptions\FailedToLoadProjectException;

class DebounceWebhookAction extends AbstractAction
{
    /**
     * This action is used to keep track duplicate Lightweight Callback Events (LCE)
     * from BigCommerce. For some reason, an event such as `store/order/created` is
     * dispatched twice. The deboucne will handle two calls, or a timeout.
     *
     * Every BigCommerce event will be debounced once (ignore second call) or timeout
     * after the debounce timeout period.
     *
     *
     * @return void
     */
	public function handle() {
        // 0 - Bootstrap the Project
        $context = resolve('context');

        $record = $this->payload->resource;

        /** @var App\Solutions\SolutionInterface $solution */
        if (!$project = $context->project($record->project)->load()){
            throw new FailedToLoadProjectException();
        }

        // Project is booted
        $project->boot();

        // Load the original event payload record
        $eventData = json_decode($record->data);
        $eventClassName = $eventData->event;

        // The unserialized record is saved with the job, but we should get it from the db
        // in case it was unloaded during the timeout period.
        $payloadRecord = Record::where('input_id', $record->input_id);

        if ($payloadRecord->count() === 0){
            return parent::handle();
        }

        $payload = new ActionPayload($payloadRecord->first());

        // Dispatch the event
        $job = new $eventClassName($payload);
        dispatch($job);

        // Call Handled Events
		parent::handle();
    }


}
