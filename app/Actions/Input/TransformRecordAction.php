<?php
namespace App\Actions\Input;

use App\Actions\AbstractAction;
use App\Actions\RunSolutionAction;
use App\Actions\ActionPayload;
use App\Records\TransformRecordHelper;
use App\Models\Record;

class TransformRecordAction extends AbstractAction
{
    /**
     * Using the Payload data, transform the Record to the USDF
     *
     * @return void
     */
	public function handle() {
        // 0 - Preload
        $inputId = $this->payload->resource->input_id;

        // 1 - Transform the record
        $helper = new TransformRecordHelper($this->payload->resource);
        $result = $helper->handle();

        // 2 - The records need to be saved because no more transformations need to be done.
        $result->save();

        // 3 - Run next if it exists
        $context = context();
        $next = $context->next($this->payload->resource);
        if ($next['project'] === false || $next['solution'] === false){
            return parent::handle();
        }

        $payloadRecord = Record::make([
            'input_id' => $inputId,
            'data' => '',
            'project' => $next['project'],
            'solution' => $next['solution']
        ]);

        $payload = new ActionPayload($payloadRecord);

        dispatch(new RunSolutionAction($payload));

        // 4 - Call Handled Events
        return parent::handle();
    }
}
