<?php
namespace App\Actions;

use App\Context;
use App\Maps\Map;
use App\Actions\AbstractAction;
use Illuminate\Support\Facades\Log;
use App\Exceptions\FailedToLoadProjectException;


class RunSolutionAction extends AbstractAction
{
    /**
     * Run the specified solution
     *
     * @return void
     */
	public function handle() {
        // 0 - Bootstrap the project handler
        $record = $this->payload->resource;
        $context = context();

        $context->setRunningSolution($record->solution);

        // Start the project
        /** @var App\Solutions\SolutionInterface $solution */
        if (!$project = $context->project($record->project)->load()){
            throw new FailedToLoadProjectException();
        }

        // Bootstrap the Project
        $project->boot();

        // Run the solution
        if (isset($record->input_id)){
            $project->run($record->solution, $record->input_id);
        } else {
            $project->run($record->solution);
        }

        // Call Handled Event
		parent::handle();
    }
}
