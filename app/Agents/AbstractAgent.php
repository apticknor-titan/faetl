<?php
namespace App\Agents;

use App\Context;

abstract class AbstractAgent implements AgentInterface
{
    /**
     * The slug string of the current Agent
     *
     * @var string
     */
    public $slug;

    /**
     * The human-readable name of the Agent
     *
     * @var string
     */
    public $name;

    /**
     * The human-readable description of the Agent
     *
     * @var string
     */
    public $description;

    /**
     * The Crontab-based schedule for this Agent to run
     *
     * @var string
     */
    public $schedule;

    /**
     * The solution slug that should be the entry point for this Agent
     *
     * @var string
     */
    public $solution;

    /**
     * Create a new instance of AgentInterface with config
     *
     * @param mixed $config
     * @return void
     */
    public function __construct($config){
        $this->setProperties($config);
    }

    /**
     * Sets all of the agents properties
     *
     * @param string[] $config
     * @return void
     */
    public function setProperties($config){
        $this->slug = $config->meta->slug;
        $this->name = $config->meta->name;
        $this->description = $config->meta->description;
        $this->schedule = $config->meta->schedule;
        $this->solution = $config->meta->solution;
    }

}
