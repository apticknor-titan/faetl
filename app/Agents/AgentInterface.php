<?php
namespace App\Agents;


/**
 * Interface AgentInterface
 *
 * @package App\Agents
 */
interface AgentInterface
{

    /**
     * Agent Handler.
     * @return bool
     */
    public function run();
}
