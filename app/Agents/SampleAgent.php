<?php


namespace App\Agents;


class SampleAgent extends AbstractAgent
{
    /**
     * The JSON object that reprsents the solution configuration.
     *
     * @var mixed[]
     */
    public $solution;

    public function __construct($solution){

    }

    /**
     * Agent Handler.
     * @return bool
     */
    public function run(){
        return true;
    }
}
