<?php
namespace App\Connectors;

use App\Context;
use Liquid\Liquid;
use Liquid\Template;
use App\Models\JobStatus;
use App\Actions\ActionPayload;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Storage;

use App\Exceptions\MethodCouldNotProceed;
use App\Actions\Input\TransformRecordAction;
use Laravel\Horizon\Contracts\JobRepository;
use Laravel\Horizon\Contracts\TagRepository;
use  Illuminate\Foundation\Bus\DispatchesJobs;

abstract class AbstractConnector implements ConnectorInterface
{
    use DispatchesJobs;

    /**
     * Path to identify an env value in a Connector config property
     */
    const SPECIAL_PROPERTY_VALUE_ENV = "env.";

    /**
     * URL Builder variable pattern
     * Based off of Postman
     */
    const PATTERN = "/{{(.*?)}}/";

    /**
     * Connector Config
     *
     * @var $config
     */
    protected $config;

    /**
     * Previous Method Result
     *
     * @var $result
     */
    protected $result;

    /**
     * Connector State
     *
     * @var $resource
     */
    protected $resource = null;

    /**
     * The ingested Records
     *
     * @var $records
     */
    protected $records;

    /**
     * An array of classes that needs cleaning
     *
     * @var string[]
     */
    public $cleaningQueue = [];

    /**
     * An ID of the parent object that needs to be cleaned
     *
     * @var string
     */
    public $cleanId = "";

    /**
     * A filename to store various caches
     *
     * @var string
     */
    public $cacheFilename = "";

    /**
     * The Liquid template engine handler
     *
     * @var Template
     */
    public $template;


    /**
     * Create a new instance of ConnectorInterface with config
     *
     * @param mixed $config
     * @return void
     */
    public function __construct($config){
        $this->setConfig($config);
        $this->setProperties($config->properties);
    }

    /**
     * Return the name of the cache file
     *  Async determines if there is a UUID appended to the filename or not.
     *
     * @param boolean $async
     * @return string
     */
    public function getCacheName($async = true){
        Log::debug('getCacheName(' . $async . ')');
        // 1 - Return cache name if it's already set.
        if (!empty($this->cacheFilename)){
            Log::debug('Cachefile: ' . $this->cacheFilename);
            return $this->cacheFilename;
        }
        // 2 - Determine the solution path
        $context = context();
        $name = $context->slug . "." . $context->getRunningSolution();

        // 3 - Name the file with a UUID or not based on async
        if ($async === true){
            Log::debug('Async is: ' . $async);
            $name = $name . $context->generateSessionId();
        }

        $this->cacheFilename = $name;
        $this->async = $async;

        return $name;
    }

    /**
     * Delete the cache file if it exists
     *
     * @return void
     */
    public function cleanCache(){
        $cacheFile = $this->cacheFilename;
        if (Storage::exists($cacheFile)){
            Storage::delete($cacheFile);
        }
    }

    /**
     * Config setter
     *
     * @param mixed $config
     * @return void
     */
    public function setConfig($config){
        $this->config = $config;
    }

    /**
     * Config getter
     *
     * @return mixed
     */
    public function getConfig(){
        return $this->config;
    }

    /**
     * Result setter
     *
     * @param mixed $result
     * @return void
     */
    public function setResult($result){
        $this->result = $result;
    }

    /**
     * Result getter
     *
     * @return mixed
     */
    public function getResult(){
        return $this->result;
    }

    /**
     * Context setter
     *
     * @param Context $context
     * @return void
     */
    public function setContext($context){
        $this->context = $context;
    }

    /**
     * Context getter
     *
     * @return Context
     */
    public function getContext(){
        return $this->context;
    }

    /**
     * Set the properties provided for the connector
     *
     * @return mixed
     */
    public function setProperties($properties){
        foreach ($properties as $property => $value){
            $this->$property = $this->resolvePropertyValue($value);
        }
    }

    /**
     * Returns all of the public controls for this connector
     *
     * @return mixed
     */
    public function getControls(){
        $controls = [];

        $self = new \ReflectionClass(self::class);
        foreach ($self->getMethods() as $method){
            $comment = $method->getDocComment();
            if (stristr($comment, '@control')){
                $controls[] = $method;
            }
        }
        return collect($controls);
    }

    /**
     * Property values can be ENV paths. Resolve them if they are env paths
     *
     * @param mixed $value
     * @return string
     */
    protected function resolvePropertyValue($value){
        if (substr($value, 0, 4) == self::SPECIAL_PROPERTY_VALUE_ENV){
            return env(strtoupper(substr($value, 4)), $value);
        }

        return $value;
    }

    /**
     * Determine if the resource is open or closed.
     *
     * @return boolean
     */
    protected function isOpen(){
        if (empty($this->resource)){
            return false;
        }

        return true;
    }

    /**
     * A blank operation.
     *
     * @control
     * @return void
     */
    public function noop(){
        return;
    }

    /**
     * Dispatch a transformation action
     *
     * @param ActionPayload $payload
     * @return void
     */
    public function queueForTransformation(ActionPayload $payload){
        $action = new TransformRecordAction($payload);
        $result = $this->dispatch($action);

        // Resolve the Job
        $jobStatus = JobStatus::where('key', $action->tag())->first();
        return $jobStatus;
    }

    /**
     * Remove all UDF records that match the input_id
     *
     * @param string $id
     * @return this
     */
    public function clean($id = null){
        if (is_null($id)){
            foreach ($this->cleaningQueue as $type){
                $type::where('input_id', $this->cleanId)->delete();
            }
            return $this;
        }
        if (class_exists($id)){
            Log::debug('' . $id . ' has been queued for cleaning.');
            $this->cleaningQueue[] = $id;
            return $this;
        }
        $this->cleanId = $id;

        return $this;
    }

    /**
     * Create or return the template object
     *
     * @return Template
     */
    protected function template(){
        if (!$this->template){
            $this->template = new Template();
        }

        return $this->template;
    }

    /**
     * Alias to Liquid's register filter function
     *
     * @param string $name
     * @param Callable $callback
     * @return void
     */
    public function registerFilter($name, $callback){
        $this->template()->registerFilter($name, $callback);
        return $this;
    }

    /**
     * Replace {{variable}} with the appropriate parts using Liquid
     *
     * @param string $template
     * @param string[] $parts
     * @return string
     */
    public function parse($template, $parts){
        $this->template()->parse($template);
        $output = $this->template()->render($parts);
        return $output;
    }

    /**
     * Remove internal platform attributes
     *
     * @param mixed $array
     * @return void
     */
    protected function removeInternalAttributes(&$array){
        unset($array['_id']);
        unset($array['input_id']);
        unset($array['output_id']);
        unset($array['created_at']);
        unset($array['updated_at']);
    }
}


