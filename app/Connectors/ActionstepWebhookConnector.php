<?php
namespace App\Connectors;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Arr;
use League\Csv\Reader;
use League\Csv\Statement;
use Webpatser\Uuid\Uuid;
use Httpful\Request;
use Illuminate\Support\Facades\Log;


use App\Exceptions\RecordsAreNotLoaded;
use App\Actions\ActionPayload;
use App\Actions\Input\TransformRecordAction;
use App\Actions\Input\DebounceWebhookAction;
use App\Models\Record;

use App\Actions\Input\BigCommerce\OrderCreatedAction;



class ActionstepWebhookConnector extends AbstractConnector
{
    /**
     * A string containing the next solution to call when the webhook is processed
     *
     * @var string
     */
    public $next;

    /**
     * Wait for input
     *
     * @return ActionstepWebhookConnector
     */
    public function open()
    {
        return $this;
    }

    /**
     * Process webhook event data from Actionstep
     *
     * @control
     * @param string $next
     * @return mixed
     */
    public function processWebhook($next = null){
        // 0 - Next is the solution to call once the webhook is processed.
        $this->next = $next;

        // 1 - Open/Load the record payload
        // 1 - Resolve event
        $request = request()->all();
        Log::debug('Callback Logged', $request);
        return;
        return $this->open();

        // 2 - Simple copy. No payload transformation required
        $this->records = $this->resource;

        return $this->records;
    }

    /**
     * Prepare a transformation event for each record
     *
     * @control
     * @return void
     */
    public function map(){
        // 1 - Make sure the records exist
        if (empty($this->records)){
            throw new RecordsAreNotLoaded();
        }

        $context = resolve('context');

        // 2 - Ingest each line item as a generic record
        foreach ($this->records as $record){
            // Wrap the record in a data element so that the map can be dynamic.
            $data = [
                'data' => $record
            ];

            $payloadRecord = Record::create([
                'input_id' => utf8_encode(Uuid::generate()),
                'data' => json_encode($data),
                'project' => $context->meta->slug,
                'solution' => $context->getRunningSolution()
            ]);

            // 3 - Prepare and dispatch the transformation action
            $payload = new ActionPayload($payloadRecord);
            $this->queueForTransformation($payload);
        }

    }

}
