<?php

namespace App\Connectors;

interface ApiConnectorInterface {
    const BIND_PATTERN = "/{{(.*?)}}/";
}
