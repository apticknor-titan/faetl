<?php
namespace App\Connectors;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Arr;
use League\Csv\Reader;
use League\Csv\Statement;
use Webpatser\Uuid\Uuid;
use Httpful\Request;

use App\Exceptions\RecordsAreNotLoaded;
use App\Actions\ActionPayload;
use App\Actions\Input\TransformRecordAction;
use App\Actions\Input\DebounceWebhookAction;
use App\Models\Record;

use App\Actions\Input\BigCommerce\OrderCreatedAction;



class BcWebhookConnector extends AbstractConnector
{
    const EVENTS = [
        'store/order/created' => OrderCreatedAction::class,
    ];

    const DEBOUNCE = [
        'store/order/created' => OrderCreatedAction::class,
    ];

    /**
     * A string containing the next solution to call when the webhook is processed
     *
     * @var string
     */
    public $next;

    /**
     * Wait for input
     *
     * @return BcWebhooksConnector
     */
    public function open()
    {
        // Attempt to bootstrap the project
        $context = resolve('context');

        // 1 - Resolve event
        $request = request()->all();
        $event = $this->getActionForEvent($request['scope']);
        $eventData = $request['data'];

        // 2 - Payload
        $data = [
            'event' => $event,
            'type' => $eventData['type'],
            'id' => $eventData['id'],
            '_original' => $request
        ];

        // 2a - Unique-ish ID builder, we cannot rely on the payload hash
        $inputId = $this->makeInputId($eventData, $request['store_id']);

        /* If the event is in the Debounce list, we should use the debounce workflow:
            # Debounce workflow:
                1. Find an existing record using the consistent ID builder
                2. If a record exists, this is the second event and thus this event should be discarded
                3. If a record does not exist, create one and dispatch the debounce event.
                4. The debounce event will delay execution for a period of time (DEBOUNCE_PERIOD)
                5. Once the period has elapsed, it will dispatch the actual event action (OrderCreatedAction)
        */

        // Create a Record
        $payloadRecord = Record::make([
            'input_id' => $inputId,
            'data' => json_encode($data),
            'project' => $context->meta->slug,
            'solution' => $this->next
        ]);

        // Prepare to dispatch the action
        $payload = new ActionPayload($payloadRecord);

        // If the action does not need to be debounced, dispatch it and return.
        if (!Arr::has(self::DEBOUNCE, $request['scope'])){
            $this->queueForRecordRetrieval($payload);
            return $this;
        }

        // The event needs to be debounced.
        // Find an existing payload record if it's been debounced already.
        $existingRecords = Record::where('input_id', $inputId);

        // If the record does not exist, this is the first event, so dispatch the debounce event
        // If the record exists, do nothing because the debounce event will execute eventually.
        if ($existingRecords->count() === 0){
            $payloadRecord->save();
            $this->queueForDebounce($payload);
        }

        return $this;
    }

    /**
     * Returns a repeatable string to represent this event
     *
     * @param string[] $data
     */
    public function makeInputId($data, $store_id){
        return 'bc_event_' . $data['type'] . '_' . $data['id'] . '_' . $store_id;
    }

    /**
     * Dispatch a debounce event
     *
     * @param ActionPayload $payload
     * @return void
     */
    public function queueForDebounce(ActionPayload $payload){
        $job = new DebounceWebhookAction($payload);
        dispatch($job->delay(5));
    }

    /**
     * Dispatch a record retrieval event
     *
     * @param ActionPayload $payload
     * @return void
     */
    public function queueForRecordRetrieval($event, ActionPayload $payload){
        $job = new $event($payload);
        dispatch($job);
    }

    /**
     * Resolve the class name for a BC Webhook event
     *
     * @param string $event
     * @return string|null
     */
    public function getActionForEvent($event){
        return self::EVENTS[$event] ?? null;
    }

    /**
     * Process the LCE from BigCommerce
     *
     * @control
     * @param string $next
     * @return mixed
     */
    public function processWebhook($next){
        // 0 - Next is the solution to call once the webhook is processed.
        $this->next = $next;

        // 1 - Open/Load the record payload
        return $this->open();

        // 2 - Simple copy. No payload transformation required
        $this->records = $this->resource;

        return $this->records;
    }

    /**
     * Prepare a transformation event for each record
     *
     * @control
     * @return void
     */
    public function map(){
        // 1 - Make sure the records exist
        if (empty($this->records)){
            throw new RecordsAreNotLoaded();
        }

        $context = resolve('context');

        // 2 - Ingest each line item as a generic record
        foreach ($this->records as $record){
            // Wrap the record in a data element so that the map can be dynamic.
            $data = [
                'data' => $record
            ];

            $payloadRecord = Record::create([
                'input_id' => utf8_encode(Uuid::generate()),
                'data' => json_encode($data),
                'project' => $context->meta->slug,
                'solution' => $context->getRunningSolution()
            ]);

            // 3 - Prepare and dispatch the transformation action
            $payload = new ActionPayload($payloadRecord);
            $this->queueForTransformation($payload);
        }

    }

}
