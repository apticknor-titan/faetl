<?php
namespace App\Connectors;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Arr;
use League\Csv\Reader;
use League\Csv\Statement;
use Webpatser\Uuid\Uuid;
use Httpful\Request;
use Httpful\Mime;
use Liquid\Liquid;
use Liquid\Template;

use App\Exceptions\RecordsAreNotLoaded;
use App\Exceptions\ApiReturnedAnError;
use App\Exceptions\BigCommerceCategoryCacheNotFound;
use App\Actions\ActionPayload;
use App\Actions\Input\TransformRecordAction;
use App\Actions\Input\DebounceWebhookAction;
use App\Models\Action;
use App\Models\Record;
use App\Models\Customer;
use App\Models\Price;
use App\Models\Address;
use App\Models\Product;
use App\Models\Image;
use App\Models\Category;
use App\Models\Attribute;
use App\Traits\ConsumesAnApi;
use App\Actions\Input\BigCommerce\OrderCreatedAction;
use App\Records\TransformRecordHelper;
use App\Context;



class BigCommerceConnector extends AbstractConnector implements ApiConnectorInterface
{
    use ConsumesAnApi;

    const BASE_URL = "https://api.bigcommerce.com/stores/{{store_hash}}/{{version}}/{{verb}}/{{id}}";
    const DEFAULT_CATEGORY = 0;
    const ENDPOINTS1 = [
        'order' => [
            'verb' => 'orders',
            'version' => 'v2'
        ],
        'product' => [
            'verb' => 'catalog/products',
            'version' => 'v3'
        ],
        'variant' => [
            'verb' => 'catalog/variants',
            'version' => 'v3'
        ]
    ];

    /**
     * A cached array of all of the customer attributes
     * @var string[]
     */
    public $customerAttributes;

    /**
     * Testing Area
     * @return void
     */
    public function sandbox(){
        $categories = $this->getCategories();
        dd($categories);

    }


    /**
     * Alias for getCachedCategoryTree($ttl);
     *
     * @param mixed $ttl
     * @return mixed
     */
    public function cacheCategoryTree($ttl = 3600, $async = true){
        // 0 - Check the cached file
        $cacheFile = $this->getCacheName($async);

        $lastModified = 0;

        if (Storage::exists($cacheFile)){
            if ((time() - Storage::lastModified($cacheFile)) < (int)$ttl){
                Log::debug('Category cache is still fresh.');
                return json_decode(Storage::get($cacheFile));
            }
        }

        Log::debug('Category cache needs refresh.');

        // 1 - Get all categories
        $categories = $this->getCategories();

        // 2 - Loop through categories and get each customfields
        foreach ($categories as $index => $category) {
            $fields = $this->getMetafields($category->id);
            $categories[$index]->remote_id = 0;

            foreach ($fields as $field){
                // 3 - The field value matches the remote id so we've found our match
                if ($field->key == "remote_id"){
                    $categories[$index]->remote_id = $field->value;
                }
            }
        }

        // 3 - Write the tree to the storage disk
        Storage::put($cacheFile, json_encode($categories));
        return $categories;
    }

    /**
     * Download the category tree from BC along with metafields and store it locally
     * This prevents making a call per category to resolve the metafields
     *
     * @param mixed $ttl
     * @return mixed
     */
    public function getCachedCategoryTree($async = false){
        // 0 - Check the cached file
        // TODO - refactor the async storage, this is a total hackjob.
        $cacheFile = $this->getCacheName($async);

        if (Storage::exists($cacheFile)){
            return json_decode(Storage::get($cacheFile));
        }

        throw new BigCommerceCategoryCacheNotFound();
    }

    /**
     * Transform the UDF to BC record
     *
     * @param mixed $previous
     * @param mixed $input_id
     */
    public function preloadCategory($previous = null, $input_id = null){
        // 0 - Init
        $context = context();

        // 1 - Load category by input_id
        // $input_id = "fdaf88b0-04a1-11ea-a947-314ddfbf6ba4"; // TODO: remove debug code
        // $input_id = "761a59a0-0573-11ea-b9cf-e345ed46d379"; // TODO: remove debug code
        $localCategory = Category::where('input_id', '=', $input_id)->get()->first();
        $categoryImage = Image::where('input_id', $input_id)->get()->first();
        if ($categoryImage){
            $localCategory->image = $categoryImage->toArray();
        }


        // 2 - Pass to next method
        return $localCategory;
    }

    public function transformCategory($previous){
        // 0 - Init
        $context = context();

        // 1 - Create a new record with the retrieved data model
        $record = Record::make([
            'input_id' => $context->generateSessionId(),
            'data' => json_encode($previous),
            'project' => $context->meta->slug,
            'solution' => $context->getRunningSolution()
        ]);

        // 2 - Transform the record
        $helper = new TransformRecordHelper($record);
        $result = $helper->handle();


        // 3 - Clean the previous records
        $this->clean($previous->input_id)
            ->clean(Image::class)
            ->clean(Attribute::class)
            ->clean(Category::class)
            ->clean();

        // Pass off to next method
        return $result;
    }

    public function uploadCategory($previous, $input_id = null){
        // 0 - Init
        $context = context();

        // 1 - Separate Category, Attributes and Images if available
        $category = Arr::first($previous->getModelsByType(Category::class));
        $attributes = $previous->getModelsByType(Attribute::class);
        $image = Arr::first($previous->getModelsByType(Image::class));

        // 2 - The remote_id is a metafield. We must have this before we can process any other data
        $remote_id = 0;
        foreach ($attributes as $attribute){
            if ($attribute->key == "remote_id"){
                $remote_id = $attribute->value;
            }
        }

        // 3 - Locate remote parent category if set
        if ($category['parent_id'] !== 0){
            $parentCategory = $this->findCategoryByRemoteId($category['parent_id']);
            $category['parent_id'] = $parentCategory->id ?? 0;
        } else {
            $category['parent_id'] = 0;
        }

        // 4 - Locate remote category if it exists.
        $remoteCategory = $this->findCategoryByRemoteId($remote_id);
        $updating = false;
        if ($remoteCategory){
            // Entity already exists, so we are "updating" it.
            // Changed is false unless we actually change a value in the cagegory.
            $updating = true;
        }

        // 5 - Create or update category
        $this->removeInternalAttributes($category);
        $categoryData = $category->toArray();

        if ($updating){
            $category = $this->updateCategory($remoteCategory->id, $categoryData);
        } else {
            // If the category does not exist, then we are not updating it.
            $category = $this->createCategory($categoryData);
        }

        $category_id = $category->id;

        // 6 - Upload Image (Optional)
        if ($image){
            $imageData = $image->toArray();
            $image = $this->createCategoryImage($category_id, $imageData);
        }

        // 7 - Create Category Metafields (Metafields can only be deleted.)
        foreach ($attributes as $field){
            $fieldData = $field->toArray();
            if ($updating){
                $attribute = $this->updateCategoryMetafield($category_id, $fieldData);
            } else {
                $attribute = $this->createCategoryMetafield($category_id, $fieldData);
            }
        }

        // 8 - Clean up everthing
        $this->clean($input_id)
                ->clean(Image::class)
                ->clean(Attribute::class)
                ->clean(Category::class)
                ->clean();
    }

    /**
     * Load the categories and prepare them for create or update
     *
     * @return mixed
     */
    public function loadCategories($previous = null, $input_id = null){
        // 2 - Determine if this category exists in BigCommerce or not
        // 2a - Local category should have a remote_id, if not search by name
        if (isset($localCategory->remote_id)){
            $remoteCategory = $this->findCategoryByRemoteId($localCategory->remote_id);
        } else {
            $remoteCategory = $this->findCategory(['name' => $localCategory->name]);
            if (count($remoteCategory) < 1){
                $remoteCategory = false;
            }
        }

        if ($remoteCategory){
            // Update the category
            return;
        }

        // 3 - Create the category
        $this->removeInternalAttributes($category);
        $category = $this->createCategory($localCategory);
        dd($category);
    }

    /**
     * Get all categories in BigCommerce, then get their metafields
     * one by one until we find one with a matching remote_id.
     *
     * If none are found, return false.
     *
     * @param string $remote_id
     * @return mixed
     */
    public function findCategoryByRemoteId($remote_id, $async = false){
        // 1 - Load categories from cache
        $categories = $this->getCachedCategoryTree($async);

        // 2 - Loop through categories and find the one with the remote_id that matches
        foreach ($categories as $index => $category) {
            if ($category->remote_id == $remote_id){
                return $category;
            }
        }

        // Nothing was found
        return false;
    }

    /**
     * Perform an API call to the endpoint for the record
     *
     * @control
     * @param mixed $previous
     * @param Record|null $record
     * @throws RecordsAreNotLoaded
     * @return mixed
     */
    public function retrieveRecord($previous = null, $record = null){
        // Determine if a record was passed in.
        if ($record == null){
            throw new RecordsAreNotLoaded();
        }
        // 1 - Open/Load the record payload
        $event = json_decode($record->data);

        // 2 - Make API Call Out
        $result = $this->request($event->type, $event->id);

        // 3 - Resolve linked records
        $data = $result->body ?? null;
        foreach ($data as $key => $value){
            if (isset($value->url)){
                $innerResult = $this->request($value->url);

                // 3a - Records that have just one element should be flattened.
                // TODO - Support multiship
                $body = $innerResult->body ?? null;
                if (is_array($body) && count($body) === 1){
                    $data->$key = reset($body);
                } else {
                    $data->$key = $body;
                }
            }
        }

        // 3b - Store original data set in the event payload.
        $event->data = $data;

        // 4 - Convert to a collection to meet the interface requirements.
        $this->records = collect([$event]);

        // 5 - Garbage collection
        $record->delete();

        // Return the record to be passed into next method
        return $this->records;
    }

    /**
     * Determine which workflow is appropriate to transform the record from BC
     *
     * @param Record|null $result
     * @param string[] $events
     * @return bool
     */
    public function dispatchTransformationWorkflow($events, $results = null){
        $context = context();

        // 1 - Loop through every record in the result
        foreach ($results as $result){
            $data = $result->data;

            // 1a - Create a new record with the retrieved data model
            $payloadRecord = Record::make([
                'input_id' => utf8_encode(Uuid::generate()),
                'data' => json_encode($data),
                'project' => $context->meta->slug,
                'solution' => self::eventForType($events, $result->type)
            ]);

            $payloadRecord->save();

            // 1b - Prepare to dispatch the action
            $payload = new ActionPayload($payloadRecord);

            // 1c - Dispatch the event
            $this->queueForTransformation($payload);
        }

        return true;
    }

    /**
     * Get all of the products ready for sending to BC
     *
     * @return void
     */
    public function prepareProducts($events, $previous = null, $input_id = null){
        // 0 - Preload
        $context = context();

        if ($input_id == null){
            throw new \Exception('Failed to receive input_id from previous method');
        }

        // 1 - Load Categories and Products
        $products = Product::where('input_id', '=', $input_id)->get();

        // 2
        $result = [];
        foreach ($products as $product){
            Log::debug('Transforming SKU: ' . $product->sku);
            // 2a - Collect all product data (attributes, images, etc..)
            $images = Image::where('input_id', '=', $product->input_id)->get();
            $attributes = Attribute::where('input_id', '=', $product->input_id)->get();
            $prices = Price::where('input_id', '=', $product->input_id)->get();

            $productData = $product->toArray();
            $productData['images'] = $images->toArray();
            $productData['attributes'] = $attributes->toArray();
            $productData['prices'] = $prices->toArray();

            // Separate categories and resolve each remote_id into a the BC ID
            $categories = explode(',', $productData['categories']);
            $productData['categories'] = [];

            foreach ($categories as $category){
                $remoteCategory = $this->findCategoryByRemoteId($category, true);
                array_push($productData['categories'], $remoteCategory->id);
            }

            // 2b - Determine which action is required based on product existence.
            $result = $this->productExists($product['sku']);
            $type = ($result ? 'update' : 'create');

            // 3 - Generate a transformation action
            $input_id = utf8_encode(Uuid::generate());
            $payloadRecord = Record::make([
                'input_id' => $input_id,
                'data' => json_encode($productData),
                'project' => $context->meta->slug,
                'solution' => self::eventForType($events, $type)
            ]);

            // 4 - Transform the record, then send it to BC
            $helper = new TransformRecordHelper($payloadRecord);
            $transformed = $helper->handle();
            $transformed->save();

            $result = $input_id;

            // 5 - Clean up previous records
            $this->clean($product->input_id)
                    ->clean(Image::class)
                    ->clean(Attribute::class)
                    ->clean(category::class)
                    ->clean(Price::class)
                    ->clean(Product::class)
                    ->clean();
        }
        return $result;
    }

    /**
     * Create products in BC
     *
     * @return void
     */
    public function loadProducts($previous = null, $input_id = null){
        // 0 - Preload
        $context = context();

        // 1
        $products = Product::where('input_id', $previous)->get();

        // 2
        foreach ($products as $product){
            Log::debug('Creating or Updating SKU in BC: ' . $product->sku);
            // 2a - Collect all product data (attributes, images, etc..)
            $images = Image::where('input_id', '=', $product->input_id)->get();
            $attributes = Attribute::where('input_id', '=', $product->input_id)->get();
            $prices = Price::where('input_id', '=', $product->input_id)->get();

            // Prepare the product data for creating/updating
            // BigCommerce requires: name, type, weight, categories and price to create a product.
            // Assemble the price
            // @todo digital/physical type definition
            $productData = $product->toArray();
            $productData['type'] = 'physical';
            $productData['price'] = $this->getDefaultPrice($prices);

            $result = $this->createOrUpdateProduct($productData);
            if (gettype($result) == "object"){
                $productId = $result->id;
            } else {
                $productId = $result['id'];
            }

            // Create, re-order or update images for a product
            $imageData = $images->toArray();
            foreach ($imageData as $image){
                $this->createOrUpdateImage($productId, $image);
            }

            // Create or update Custom Fields for a product
            $attributeData = $attributes->toArray();
            foreach ($attributeData as $attribute){
                // Drop empty attributes
                if (empty($attribute['value'])){
                    continue;
                }
                $this->createOrUpdateCustomField($productId, $attribute);
            }

            // Create or update Bulk Price rules for a product
            $priceData = $prices->toArray();
            foreach ($priceData as $price){
                if ($price['quantity_min'] > 0){
                    $this->createOrUpdatePriceRule($productId, $price);
                }
            }

            // Clean up previous records
            $this->clean($product->input_id)
                    ->clean(Image::class)
                    ->clean(Attribute::class)
                    ->clean(Price::class)
                    ->clean(Product::class)
                    ->clean();
        }

        return;
    }

    /**
     * Load customers from UDF and prepare them for transformation
     *
     * @return string[]
     */
    public function loadCustomers(){
        // 0 - Preload
        $context = context();

        // 1
        // TODO refactor the output ID slug to be the connector slug
        $customers = Customer::where('output_id', 'bigcommerce')->get();

        // 2 - Loop through each customer and load the related records
        foreach ($customers as $key => $customer){
            Log::debug('Working on customer: ' . $customer->input_id);
            // Reject customers without an email
            if (!isset($customer->email)){
                // TODO Notify of invalid customer record
                Log::debug('Customer record missing email.');
                continue;
            }

            // 2a - Collect all customer data (Addresses, attributes, etc...);
            $addresses = Address::where('input_id', '=', $customer->input_id)->get();
            $attributes = Attribute::where('input_id', '=', $customer->input_id)->get();

            // 2b - Combine all data models
            $customerData = $customer->toArray();
            foreach ($addresses as $address){
                // Address should have the require fields. If not, discard the record

                $customerData['addresses'][] = $address->toArray();
            }

            foreach ($attributes as $attribute){
                $customerData['attributes'][] = $attribute->toArray();
            }

            // 2c - Collect the results;
            $result[] = $customerData;
        }



        // Pass off to the next method
        return $result;
    }

    protected function validateAddressData($addressData){
        $required = [
            "first_name",
            "city",
            "country_code",
            "state_or_province",
            "last_name",
            "address1",
            "postal_code"
        ];

        foreach ($required as $key){
            if (array_key_exists($key, $addressData)){

            }
        }
    }

    /**
     * Receive the loaded customers from UDF and transform them to BC
     *
     * @param string[] $previous
     * @return string[]
     */
    public function transformCustomers($previous = null){
        // 0 - Preload/Init
        $context = context();
        $result = [];

        // 1 - Loop through every customer and transform it
        foreach ($previous as $customer){
            Log::debug('Transforming customer: ' . $customer['email'] ?? $customer['input_id']);
            // Use one ID for this transformation
            $input_id = Context::generateSessionId();

            // Create a Record
            $record = Record::make([
                'input_id' => $input_id,
                'data' => json_encode($customer),
                'project' => $context->slug,
                'solution' => $context->solution
            ]);

            // Transform the record
            $helper = new TransformRecordHelper($record);
            $transformed = $helper->handle();

            // Check for failed transformation
            if ($transformed->failed()){
                // TODO Notify of failed transformation
                dump($transformed);
                continue;
            }

            // Save and continue
            $transformed->save();

            $result[] = $input_id;

            // Clean up previous records
            Log::debug('Cleaning current record');
            $this->clean($customer['input_id'])
                ->clean(Customer::class)
                ->clean(Address::class)
                ->clean(Attribute::class)
                ->clean();
        }

        // 2 - Pass the collection to the next method
        return $result;
    }

    public function skipTransformCustomers(){
        $customers = Customer::all();
        foreach ($customers as $customer){
            $result[] = $customer->input_id;
        }
        return $result;
    }

    /**
     * Create new customer attributes
     *
     * @param string[] $previous The list of input_ids from the previous method
     * @return string[] The exact same list of IDs
     */
    public function uploadCustomerAttributes($previous = null){
        // 1 - Load remote attributes
        $remoteAttributes = $this->cacheCustomerAttributes();
        $attributes = Attribute::all();

        $stack = [];
        foreach ($attributes as $attribute){
            $stack[$attribute->name] = $attribute;
        }

        foreach ($stack as $attribute){
            $attributeData = $attribute->toArray();

            if (!isset($remoteAttributes[$attribute->name])){
                $response = $this->createCustomerAttribute($attributeData);
                $remoteAttribute = reset($response);
                $this->customerAttributes[$attribute->name] = $remoteAttribute->id;
            }
        }

        return $previous;
    }

    /**
     * Upload transformed customers from the previous method
     *
     * @return mixed
     */
    public function uploadCustomers($previous = null){
        // 0 - Preload
        $context = context();
        $createCustomerBatch = [];

        // Cache the customer attributes
        $remoteAttributes = $this->cacheCustomerAttributes();

        // 1 - Load customers based on previous ID
        foreach ($previous as $input_id){
            $customer = Customer::where('input_id', $input_id)->get()->first();
            $addresses = Address::where('input_id', $input_id)->get();
            $attributes = Attribute::where('input_id', $input_id)->get();

            $customerData = $customer->toArray();

            // 2 - Find or Create customer
            $remoteCustomer = $this->getCustomerByEmail($customer->email);
            if (empty($remoteCustomer)){
                Log::debug('Did not find remote customer for ' . $customer->email . '.');

                // 2a - Remove internal attributes
                $this->removeInternalAttributes($customerData);

                // 2b - Add Address to payload
                foreach ($addresses as $address){
                    $addressData = $address->toArray();
                    // TODO - Add this to the map
                    $addressData['country_code'] = 'US';
                    $customerData['addresses'][] = $addressData;
                }

                // 2c - Add attribute data
                foreach ($attributes as $attribute){
                    $attributeData = $attribute->toArray();
                    $remoteId = $remoteAttributes[$attribute->name];
                    $customerData['attributes'][] = [
                        'attribute_id' => $remoteId,
                        'attribute_value' => $attribute->value
                    ];
                }

                // 2d - Add to the batch for processing after the fact.
                $createCustomerBatch[] = $customerData;
            } else {    /** else empty($remoteCustomer) */
                // Do update
            }

        }

        // 3 - Perform batches of customer creation
        if (!empty($createCustomerBatch)){
            // BigCommerce will only perform 10 customers at a time.
            $batches = array_chunk($createCustomerBatch, 10);
            foreach ($batches as $batch){
                $this->createCustomer($batch, true);
            }

        }

        // End of solution
        return;
    }

    /**
     * Make a request to BC API and check for product existence.
     *
     * @param string $product_id
     * @return Product|null
     */
    public function productExists($product_id){
        // 1 - Perform request
        $result = $this->findProduct(['sku' => $product_id]);

        // 2 - Parse data
        return $this->entityExists($result);
    }

    /**
     * Make a request to BC API and check for category existence.
     *
     * @param string $name
     * @return Category|null
     */
    public function categoryExists($name){
        // 1 - Perform request
        $result = $this->findCategory(['name' => $name]);

        // 2 - Parse data
        return $this->entityExists($result);
    }

    /**
     * Make a request to BC API and check for category existence.
     *
     * @param string $name
     * @return Category|null
     */
    public function findOrCreateCategory($name, $parent_id = 0){
        $category = [
            'parent_id' => $parent_id,
            'name' => $name
        ];

        // 1 - Perform request
        $result = $this->findCategory($category);

        // 2 - Parse data
        $result = $this->entityExists($result);

        // 3 - If category already exists, return it.
        if ($result){
            // Data is returned as an array, but there can only be one category
            // so grab the first element of the array.
            return reset($result);
        }

        // 4 - Create category
        $result = $this->createCategory($category);

        // The result is not returned as an array this time because you can only create one category at a time.
        return (array)$result->data;
    }

    /**
     * Create a new product, or find and update an existing product
     *
     * @param string[] $productData
     * @return Product|null
     */
    public function createOrUpdateProduct($productData){
        // 1 - Attempt to find the product via SKU
        $result = $this->findProduct(['sku' => $productData['sku']]);

        // 2 - Parse data
        $result = $this->entityExists($result);
        if ($result){
            // Update product
            $existingData = reset($result);
            return $this->prepareToUpdateProduct($existingData, $productData);
        }

        // Create the product since it does not exist.
        $result = $this->createProduct($productData);
        return (array)$result->data;
    }

    /**
     * Compare each element in Updated with Existing, then update the product in BC as necessary
     *
     * @param string[] $existing
     * @param string[] $updated
     * @return Product|null
     */
    public function prepareToUpdateProduct($existing, $updated){
        // 0 - Strip the internal attributes
        $this->removeInternalAttributes($updated);

        $payload = [];

        // 1 - Loop through each Updated value and mark it as updated or not
        foreach ($updated as $key => $value){
            if ($existing[$key] != $value){
                $payload[$key] = $value;
            }
        }

        // 2 - Don't do anything if no changes were made
        if (count($payload) === 0){
            return $existing;
        }

        // 3 - Changes were made. PUT them to the server
        return $this->updateProduct($existing['id'], $payload);
    }

    /**
     * Entity existence is checked by either returning the model or false
     *
     * @param string[] $result
     * @return string[]|false
     */
    public function entityExists($result){
        $data = json_decode(json_encode($result), true);
        if (Arr::get($data, 'meta.pagination.count') !== 0){
            return $data['data'];
        }
        return false;
    }

    /**
     * Replace {{variable}} with the appropriate URL parts
     *
     * @param string $base
     * @param string[] $parts
     * @return string
     */
    public function buildUrl($base, $parts){
        $callback = function($matches) use ($parts){
            return $parts[$matches[1]] ??  '';
        };
        return preg_replace_callback(self::PATTERN, $callback, $base);
    }

    /**
     * Return the event for the type
     * Ex. 'order' => 'wh_map_order'
     *
     * @param string $type
     * @return string
     */
    public static function eventForType($events, $type){
        foreach ($events as $event){
            if (isset($event->$type)){
                return $event->$type;
            }
        }
    }

    /**
     * The default price should be the first simple price in the list.
     *
     * The rest of the prices should be Tier/Bulk/Complex prices
     *
     *
     * @deprecated
     * @param Price[] $prices
     * @return float
     */
    public function getDefaultPrice($prices){
        foreach ($prices as $price){
            if ($price->quantity_min === 0){
                return $price->amount;
            }
        }
    }

    public $endpoints = [
        "base_url" => "https://api.bigcommerce.com/stores/{{store_hash}}/",
        "customer" => [
            "find" => "v3/customers?{{search_query}}",
            "create" => "v3/customers",
            "read-all" => "v3/customers?limit=250&page={{page}}",
            "read" => "v3/customers/{{category_id}}",
            "update" => "v3/customers/{{category_id}}",
            "delete" => "v3/customers/{{category_id}}",
        ],
        "customer-attribute" => [
            "find" => "v3/customers/attributes?{{search_query}}",
            "create" => "v3/customers/attributes",
            "read-all" => "v3/customers/attributes?limit=250&page={{page}}",
            "delete" => "v3/customers/attributes/{{attribute_id}}",
        ],
        "product" => [
            "find" => "v3/catalog/products?{{search_query}}",
            "create" => "v3/catalog/products",
            "read-all" => "v3/catalog/products?limit=250&page={{page}}",
            "read" => "v3/catalog/products/{{product_id}}",
            "update" => "v3/catalog/products/{{product_id}}?include_fields={{include_fields}}",
            "delete" => "v3/catalog/products/{{product_id}}",
        ],
        "customfields" => [
            "create" => "v3/catalog/products/{{product_id}}/custom-fields",
            "read-all" => "v3/catalog/products/{{product_id}}/custom-fields",
            "read" => "v3/catalog/products/{{product_id}}/custom-fields/{{field_id}}",
            "update" => "v3/catalog/products/{{product_id}}/custom-fields/{{field_id}}",
            "delete" => "v3/catalog/products/{{product_id}}/custom-fields/{{field_id}}",
        ],
        "productimages" => [
            "create" => "v3/catalog/products/{{product_id}}/images",
            "read-all" => "v3/catalog/products/{{product_id}}/images",
            "read" => "v3/catalog/products/{{product_id}}/images/{{image_id}}",
            "update" => "v3/catalog/products/{{product_id}}/images/{{image_id}}",
            "delete" => "v3/catalog/products/{{product_id}}/images/{{image_id}}",
        ],
        "bulkprice" => [
            "create" => "v3/catalog/products/{{product_id}}/bulk-pricing-rules",
            "read-all" => "v3/catalog/products/{{product_id}}/bulk-pricing-rules",
            "read" => "v3/catalog/products/{{product_id}}/bulk-pricing-rules/{{rule_id}}",
            "update" => "v3/catalog/products/{{product_id}}/bulk-pricing-rules/{{rule_id}}",
            "delete" => "v3/catalog/products/{{product_id}}/bulk-pricing-rules/{{rule_id}}",
        ],
        "category" => [
            "find" => "v3/catalog/categories?{{search_query}}",
            "create" => "v3/catalog/categories",
            "read-all" => "v3/catalog/categories?limit=250&page={{page}}",
            "read" => "v3/catalog/categories/{{category_id}}",
            "update" => "v3/catalog/categories/{{category_id}}",
            "delete" => "v3/catalog/categories/{{category_id}}",
        ],
        "category-image" => [
            "create" => "v3/catalog/categories/{{category_id}}/image",
            "delete" => "v3/catalog/categories/{{category_id}}/image",
        ],
        "category-metafield" => [
            "create" => "v3/catalog/categories/{{category_id}}/metafields",
            "read-all" => "v3/catalog/categories/{{category_id}}/metafields",
            "read" => "v3/catalog/categories/{{category_id}}metafields/{{field_id}}",
            "update" => "v3/catalog/categories/{{category_id}}/metafields/{{field_id}}",
            "delete" => "v3/catalog/categories/{{category_id}}/metafields/{{field_id}}",
        ],
    ];

    /**
     * Build the URL from the endpoint
     *
     * @param string $endpoint
     * @param string[]|mixed $parameters
     * @return string
     */
    public function makeUrl($endpoint, $parameters = []){
        if (!isset($parameters['store_hash'])) {
            $parameters['store_hash'] = $this->store_hash;
        }
        $entries = array_dot($this->endpoints);
        return $this->parse($entries['base_url'] . $entries[$endpoint], $parameters);
    }

    /**
     * Parse the response from the API
     *
     * @todo Add better error handling
     * @param  $response
     * @return mixed
     */
    public function parseApiResponse($response){
        if (!isset($response->body)) {
            throw new ApiReturnedAnError($response);
        }

        if (isset($response->body->status) &&
                $response->body->status != 200){
            throw new ApiReturnedAnError($response);
        }

        return $response->body;
    }

    /**
     * Parse the paged response from the API
     *
     * @todo Add better error handling
     * @param  $response
     * @return mixed
     */
    public function parsePagedApiResponse(&$data, $response){
        if (!isset($response->body)) {
            throw new ApiReturnedAnError($response);
        }

        if (isset($response->body->status) &&
                $response->body->status != 200){
            throw new ApiReturnedAnError($response);
        }

        $tempData = (array)$response->body->data;
        $data = json_decode(json_encode(array_merge((array)$data, $tempData)));

        if ($response->body->meta->pagination->current_page == $response->body->meta->pagination->total_pages || $response->body->meta->pagination->total_pages === 0){
            return false;
        }

        return $response->body->meta->pagination->current_page + 1;
    }

    /**
     * Create a product in BigCommerce
     *
     * @param string[] $product
     * @return void
     */
    public function createProduct($product){
        $url = $this->makeUrl("product.create");

        $this->removeInternalAttributes($product);

        $payload = json_encode($product);

        return $this->parseApiResponse(Request::post($url)
                    ->expectsJson()
                    ->sendsJson()
                    ->withXAuthClient($this->auth_client)
                    ->withXAuthToken($this->auth_token)
                    ->body($payload)
                    ->send());
    }

    /**
     * Update a product in BigCommerce
     *
     * @param string[] $product
     * @return void
     */
    public function updateProduct($product_id, $productData){
        $include_fields = join(',', array_keys($productData));
        $url = $this->makeUrl("product.update", ['product_id' => $product_id, 'include_fields' => $include_fields]);

        $this->removeInternalAttributes($productData);

        $payload = json_encode($productData);

        return $this->parseApiResponse(Request::put($url)
                    ->expectsJson()
                    ->sendsJson()
                    ->withXAuthClient($this->auth_client)
                    ->withXAuthToken($this->auth_token)
                    ->body($payload)
                    ->send());
    }

    /**
     * Find a Product in BigCommerce
     *
     * @param string $query
     * @return mixed
     */
    public function findProduct($query){
        $url = $this->makeUrl("product.find", ['search_query' => http_build_query($query)]);

        return $this->parseApiResponse(Request::get($url)
                    ->expectsJson()
                    ->sendsJson()
                    ->withXAuthClient($this->auth_client)
                    ->withXAuthToken($this->auth_token)
                    ->send());
    }

    /**
     * Get a category from BigCommerce
     *
     * @param string $category_id
     * @return mixed
     */
    public function getCategory($category_id){
        $url = $this->makeUrl("category.read", ['category_id' => $category_id]);

        return $this->parseApiResponse(Request::get($url)
                    ->expectsJson()
                    ->sendsJson()
                    ->withXAuthClient($this->auth_client)
                    ->withXAuthToken($this->auth_token)
                    ->send());
    }

    /**
     * Get all categories from BigCommerce
     *
     * @return mixed
     */
    public function getCategories(){
        $page = 1;
        $data = [];

        while ($page){
            $url = $this->makeUrl("category.read-all", ['page' => $page]);

            $page = $this->parsePagedApiResponse($data, Request::get($url)
                        ->expectsJson()
                        ->sendsJson()
                        ->withXAuthClient($this->auth_client)
                        ->withXAuthToken($this->auth_token)
                        ->send());
        }

        return $data;
    }

    /**
     * Find a Category in BigCommerce
     *
     * @param string $query
     * @return mixed
     */
    public function findCategory($query){
        $url = $this->makeUrl("category.find", ['search_query' => http_build_query($query)]);

        return $this->parseApiResponse(Request::get($url)
                    ->expectsJson()
                    ->sendsJson()
                    ->withXAuthClient($this->auth_client)
                    ->withXAuthToken($this->auth_token)
                    ->send());
    }


    /**
     * Create a Category in BigCommerce
     *
     * @param string $query
     * @return mixed
     */
    public function createCategory($category){
        $url = $this->makeUrl("category.create");

        $body = json_encode($category);

        $response = $this->parseApiResponse(Request::post($url)
                    ->expectsJson()
                    ->sendsJson()
                    ->withXAuthClient($this->auth_client)
                    ->withXAuthToken($this->auth_token)
                    ->body($body)
                    ->send());

        return $response->data;
    }


    /**
     * Update a Category in BigCommerce
     *
     * @param string $query
     * @return mixed
     */
    public function updateCategory($category_id, $category){
        $url = $this->makeUrl("category.update", ['category_id' => $category_id]);

        $body = json_encode($category);

        $response = $this->parseApiResponse(Request::put($url)
                    ->expectsJson()
                    ->sendsJson()
                    ->withXAuthClient($this->auth_client)
                    ->withXAuthToken($this->auth_token)
                    ->body($body)
                    ->send());

        return $response->data;
    }

    /**
     * Get the metafields associated with a category.
     *
     * @param string $category_id
     * @return string[]|null
     */
    public function getMetafields($category_id){
        $url = $this->makeUrl("category-metafield.read-all", ['category_id' => $category_id]);

        $response = $this->parseApiResponse(Request::get($url)
                    ->expectsJson()
                    ->sendsJson()
                    ->withXAuthClient($this->auth_client)
                    ->withXAuthToken($this->auth_token)
                    ->send());

        return $response->data;
    }

    /**
     * Get a metafield associated with a category.
     *
     * @param string $category_id
     * @param string $field_id
     * @return string[]|null
     */
    public function getMetafield($category_id, $field_id){
        $url = $this->makeUrl("category-metafield.read", ['category_id' => $category_id, 'field_id' => $field_id]);

        return $this->parseApiResponse(Request::get($url)
                    ->expectsJson()
                    ->sendsJson()
                    ->withXAuthClient($this->auth_client)
                    ->withXAuthToken($this->auth_token)
                    ->send());
    }

    /**
     * Create a metafield and associate with a category
     *
     * @param string $category_id
     * @param string[] $fieldData
     * @return string[]|null
     */
    public function createMetafield($category_id, $fieldData){
        $url = $this->makeUrl("category-metafield.create", ['category_id' => $category_id]);

        $this->removeInternalAttributes($fieldData);
        $body = json_encode($fieldData);

        return $this->parseApiResponse(Request::post($url)
                    ->expectsJson()
                    ->sendsJson()
                    ->withXAuthClient($this->auth_client)
                    ->withXAuthToken($this->auth_token)
                    ->body($body)
                    ->send());
    }

    /**
     * Update a metafield with fieldData
     *
     * @param string $category_id
     * @param string $field_id
     * @param string $fieldData
     * @return string[]|null
     */
    public function updateMetafield($category_id, $field_id, $fieldData){
        $url = $this->makeUrl("category-metafield.update", ['category_id' => $category_id, 'field_id' => $field_id]);

        $this->removeInternalAttributes($fieldData);
        $body = json_encode($fieldData);

        return $this->parseApiResponse(Request::put($url)
                    ->expectsJson()
                    ->sendsJson()
                    ->withXAuthClient($this->auth_client)
                    ->withXAuthToken($this->auth_token)
                    ->body($fieldData)
                    ->send());
    }

    /**
     * Delete a metafield
     *
     * @param string $category_id
     * @param string $field_id
     * @return string[]|null
     */
    public function deleteMetafield($category_id, $field_id){
        $url = $this->makeUrl("category-metafield.delete", ['category_id' => $category_id, 'field_id' => $field_id]);

        return $this->parseApiResponse(Request::get($url)
                    ->expectsJson()
                    ->sendsJson()
                    ->withXAuthClient($this->auth_client)
                    ->withXAuthToken($this->auth_token)
                    ->send());
    }

    /**
     * Create a new product image, or find and update an existing product image
     *
     * @param string $product_id
     * @param string[] $imageData
     * @return Product|null
     */
    public function createOrUpdateImage($product_id, $imageData){
        // 1 - Attempt to find the images
        $images = $this->getProductImages($product_id);

        // 2 - Parse data
        $result = $this->entityExists($images);
        if ($result){
            foreach ($result as $image){
                if (stristr($image['image_file'], pathinfo($imageData['image_file'], PATHINFO_FILENAME))){
                    return $image;
                }
            }
        }

        // Create the image since it does not exist.
        $result = $this->createProductImage($product_id, $imageData);
        return (array)$result->data;
    }


    /**
     * Get all images for a product.
     *
     * @param string $product_id
     * @return mixed
     */
    public function getProductImages($product_id){
        $url = $this->makeUrl("productimages.read-all", ['product_id' => $product_id]);

        return $this->parseApiResponse(Request::get($url)
                    ->expectsJson()
                    ->sendsJson()
                    ->withXAuthClient($this->auth_client)
                    ->withXAuthToken($this->auth_token)
                    ->send());
    }

    /**
     * Create an image for a product.
     *
     * @param string $product_id
     * @param string[] $imageData
     * @return mixed
     */
    public function createProductImage($product_id, $imageData){
        $url = $this->makeUrl("productimages.create", ['product_id' => $product_id]);

        // Write the image data to a file temporarily
        $files['image_file'] = sys_get_temp_dir() . DIRECTORY_SEPARATOR . $imageData['image_file'];
        file_put_contents($files['image_file'], base64_decode($imageData['data']));
        unset($imageData['data']);

        $this->removeInternalAttributes($imageData);

        return $this->parseApiResponse(Request::post($url)
                    ->expectsJson()
                    ->sendsType(Mime::FORM)
                    ->withXAuthClient($this->auth_client)
                    ->withXAuthToken($this->auth_token)
                    ->attach($files)
                    ->send());
    }

    /**
     * Create a new product custom field, or find and update an existing custom field
     *
     * @param string $product_id
     * @param string[] $attributeData
     * @return Product|null
     */
    public function createOrUpdateCustomField($product_id, $attributeData){
        // 1 - Attempt to find the custom field
        $fields = $this->getCustomFields($product_id);

        // 2 - Parse data
        $result = $this->entityExists($fields);
        if ($result){
            foreach ($result as $field){
                if ($field['name'] == $attributeData['name']
                    && $field['value'] != $attributeData['value']){
                    return $this->prepareToUpdateCustomField($product_id, $field, $attributeData);
                }
            }
            return $result;
        }

        // Create the image since it does not exist.
        $result = $this->createCustomField($product_id, $attributeData);
        return (array)$result->data;
    }

    /**
     * Compare each element in Updated with Existing, then update the product in BC as necessary
     *
     * @param string[] $existing
     * @param string[] $updated
     * @return Product|null
     */
    public function prepareToUpdateCustomField($product_id, $existing, $updated){
        // 0 - Strip the internal attributes
        $this->removeInternalAttributes($updated);

        $payload = [];

        // 1 - Loop through each Updated value and mark it as updated or not
        foreach ($updated as $key => $value){
            if ($existing[$key] != $value){
                $payload[$key] = $value;
            }
        }

        // 2 - Don't do anything if no changes were made
        if (count($payload) === 0){
            return $existing;
        }

        // 3 - Changes were made. PUT them to the server
        $existing_id = $existing['id'];
        return $this->updateCustomField($product_id, $existing_id, $payload);
    }

    /**
     * Create a custom field for a product
     *
     * @param string $product_id
     * @param string[] $fieldData
     * @return mixed
     */
    public function createCustomField($product_id, $fieldData){
        $url = $this->makeUrl("customfields.create", ['product_id' => $product_id]);

        $this->removeInternalAttributes($fieldData);

        return $this->parseApiResponse(Request::post($url)
                    ->expectsJson()
                    ->sendsJson()
                    ->withXAuthClient($this->auth_client)
                    ->withXAuthToken($this->auth_token)
                    ->body($fieldData)
                    ->send());
    }

    /**
     * Get all custom fields for a product.
     *
     * @param string $product_id
     * @return mixed
     */
    public function getCustomFields($product_id){
        $url = $this->makeUrl("customfields.read-all", ['product_id' => $product_id]);

        return $this->parseApiResponse(Request::get($url)
                    ->expectsJson()
                    ->sendsJson()
                    ->withXAuthClient($this->auth_client)
                    ->withXAuthToken($this->auth_token)
                    ->send());
    }

    /**
     * Update a custom field for a product
     *
     * @param string $product_id
     * @param string[] $fieldData
     * @return mixed
     */
    public function updateCustomField($product_id, $field_id, $fieldData){
        $url = $this->makeUrl("customfields.update", ['product_id' => $product_id, 'field_id' => $field_id]);

        $this->removeInternalAttributes($fieldData);

        return $this->parseApiResponse(Request::put($url)
                    ->expectsJson()
                    ->sendsJson()
                    ->withXAuthClient($this->auth_client)
                    ->withXAuthToken($this->auth_token)
                    ->body($fieldData)
                    ->send());
    }

    /**
     * Create a new product bulk price rule, or find and update an existing bulk price rule
     *
     * @param string $product_id
     * @param string[] $priceData
     * @return Product|null
     */
    public function createOrUpdatePriceRule($product_id, $priceData){
        // 1 - Attempt to find the price rule
        $fields = $this->getPriceRules($product_id);

        // 2 - Parse data
        $result = $this->entityExists($fields);
        if ($result){
            foreach ($result as $price){
                dump($price);
                // if ($field['name'] == $attributeData['name']
                //     && $field['value'] != $attributeData['value']){
                //     return $this->prepareToUpdateCustomField($product_id, $field, $attributeData);
                // }
            }
            return $result;
        }

        // Create the price rule since it does not exist.
        $result = $this->createPriceRule($product_id, $priceData);
        return (array)$result->data;
    }

    /**
     * Compare each element in Updated with Existing, then update the price rule in BC as necessary
     *
     * @param string[] $existing
     * @param string[] $updated
     * @return Product|null
     */
    public function prepareToUpdatePriceRule($product_id, $existing, $updated){
        // 0 - Strip the internal attributes
        $this->removeInternalAttributes($updated);

        $payload = [];

        // 1 - Loop through each Updated value and mark it as updated or not
        foreach ($updated as $key => $value){
            if ($existing[$key] != $value){
                $payload[$key] = $value;
            }
        }

        // 2 - Don't do anything if no changes were made
        if (count($payload) === 0){
            return $existing;
        }

        // 3 - Changes were made. PUT them to the server
        $existing_id = $existing['id'];
        return $this->updatePriceRule($product_id, $existing_id, $payload);
    }

    /**
     * Create a bulk price rule for a product
     *
     * @param string $product_id
     * @param string[] $priceData
     * @return mixed
     */
    public function createPriceRule($product_id, $priceData){
        $url = $this->makeUrl("bulkprice.create", ['product_id' => $product_id]);

        $this->removeInternalAttributes($priceData);

        return $this->parseApiResponse(Request::post($url)
                    ->expectsJson()
                    ->sendsJson()
                    ->withXAuthClient($this->auth_client)
                    ->withXAuthToken($this->auth_token)
                    ->body($priceData)
                    ->send());
    }

    /**
     * Get all price rules for a product.
     *
     * @param string $product_id
     * @return mixed
     */
    public function getPriceRules($product_id){
        $url = $this->makeUrl("bulkprice.read-all", ['product_id' => $product_id]);

        return $this->parseApiResponse(Request::get($url)
                    ->expectsJson()
                    ->sendsJson()
                    ->withXAuthClient($this->auth_client)
                    ->withXAuthToken($this->auth_token)
                    ->send());
    }

    /**
     * Update a price rule for a product
     *
     * @param string $product_id
     * @param string[] $fieldData
     * @return mixed
     */
    public function updatePriceRule($product_id, $rule_id, $priceData){
        $url = $this->makeUrl("bulkprice.update", ['product_id' => $product_id, 'rule_id' => $rule_id]);

        $this->removeInternalAttributes($priceData);

        return $this->parseApiResponse(Request::put($url)
                    ->expectsJson()
                    ->sendsJson()
                    ->withXAuthClient($this->auth_client)
                    ->withXAuthToken($this->auth_token)
                    ->body($priceData)
                    ->send());
    }

        /**
     * Create an image for a category.
     *
     * @param string $category_id
     * @param string[] $imageData
     * @return mixed
     */
    public function createCategoryImage($category_id, $imageData){
        $url = $this->makeUrl("category-image.create", ['category_id' => $category_id]);

        // Write the image data to a file temporarily
        $files['image_file'] = sys_get_temp_dir() . DIRECTORY_SEPARATOR . $imageData['image_file'];
        file_put_contents($files['image_file'], base64_decode($imageData['data']));
        unset($imageData['data']);

        $this->removeInternalAttributes($imageData);

        $response = $this->parseApiResponse(Request::post($url)
                    ->expectsJson()
                    ->sendsType(Mime::FORM)
                    ->withXAuthClient($this->auth_client)
                    ->withXAuthToken($this->auth_token)
                    ->attach($files)
                    ->send());

        return $response->data;
    }

    /**
     * Create a custom field for a category
     *
     * @param string $category_id
     * @param string[] $fieldData
     * @return mixed
     */
    public function createCategoryMetafield($category_id, $fieldData){
        $url = $this->makeUrl("category-metafield.create", ['category_id' => $category_id]);

        $this->removeInternalAttributes($fieldData);

        return $this->parseApiResponse(Request::post($url)
                    ->expectsJson()
                    ->sendsJson()
                    ->withXAuthClient($this->auth_client)
                    ->withXAuthToken($this->auth_token)
                    ->body($fieldData)
                    ->send());
    }

    /**
     * Create a custom field for a category
     *
     * @param string $category_id
     * @param string[] $fieldData
     * @return mixed
     */
    public function updateCategoryMetafield($category_id, $fieldData){
        return;
        $url = $this->makeUrl("category-metafield.create", ['category_id' => $category_id]);

        $this->removeInternalAttributes($fieldData);

        return $this->parseApiResponse(Request::post($url)
                    ->expectsJson()
                    ->sendsJson()
                    ->withXAuthClient($this->auth_client)
                    ->withXAuthToken($this->auth_token)
                    ->body($fieldData)
                    ->send());
    }

    /**
     * CUSTOMER SECTION START
     */

    /**
     * Find a Customer in BigCommerce
     *
     * @param string $query
     * @return mixed
     */
    public function findCustomer($query){
        $url = $this->makeUrl("customer.find", ['search_query' => http_build_query($query)]);

        return $this->parseApiResponse(Request::get($url)
                    ->expectsJson()
                    ->sendsJson()
                    ->withXAuthClient($this->auth_client)
                    ->withXAuthToken($this->auth_token)
                    ->send())->data;
    }

    /**
     * Get a Customer by ID from BigCommerce
     *
     * @param string $customer_id
     * @return mixed
     */
    public function getCustomerById($customer_id){
        return $this->findCustomer(['id:in' => $customer_id]);
    }

    /**
     * Get a Customer by Email from BigCommerce
     *
     * @param string $customer_email
     * @return mixed
     */
    public function getCustomerByEmail($customer_email){
        return $this->findCustomer(['email:in' => $customer_email]);
    }

    /**
     * Create a customer
     *
     * @param string[] $customerData
     * @return mixed
     */
    public function createCustomer($customerData, $batch = false){
        $url = $this->makeUrl("customer.create");

        // Body must be an array if not a batch of customers
        $body = json_encode($customerData);
        if (!$batch){
            $body = '['.$body.']';
        }


        $response = $this->parseApiResponse(Request::post($url)
                    ->expectsJson()
                    ->sendsJson()
                    ->withXAuthClient($this->auth_client)
                    ->withXAuthToken($this->auth_token)
                    ->body($body)
                    ->send());

        return $response->data;
    }

    /**
     * CUSTOMER SECTION END
     */


    /**
     * CUSTOMER ATTRIBUTE SECTION START
     */

    /**
     * Download all of the customer attributes and create a Name > ID associative array
     */
    public function cacheCustomerAttributes(){
        // 0 - Check for existing attributes
        if (!empty($this->customerAttributes)){
            return $this->customerAttributes;
        }

        $stack = [];

        // 1 - Get all of the attributes
        $attributes = $this->getCustomerAttributes();

        // 2 - Loop through each and make the array
        foreach ($attributes as $attribute){
            $stack[$attribute->name] = $attribute->id;
        }

        // 3 - Save them
        $this->customerAttributes = $stack;

        return $stack;
    }

    /**
     * Get all customer attributes
     *
     * @return mixed
     */
    public function getCustomerAttributes(){
        $page = 1;
        $data = [];

        while ($page){
            $url = $this->makeUrl("customer-attribute.read-all", ['page' => $page]);

            $page = $this->parsePagedApiResponse($data, Request::get($url)
                        ->expectsJson()
                        ->withXAuthClient($this->auth_client)
                        ->withXAuthToken($this->auth_token)
                        ->send());

        }

        return $data;
    }

    /**
     * Create a customer attribute
     *
     * @param string[] $attributeData
     * @return mixed
     */
    public function createCustomerAttribute($attributeData){
        $url = $this->makeUrl("customer-attribute.create");

        $body = '['. json_encode($attributeData) . ']';

        $response = $this->parseApiResponse(Request::post($url)
                    ->expectsJson()
                    ->sendsJson()
                    ->withXAuthClient($this->auth_client)
                    ->withXAuthToken($this->auth_token)
                    ->body($body)
                    ->send());

        return $response->data;
    }



    /**
     * CUSTOMER ATTRIBUTE SECTION END
     */

}
