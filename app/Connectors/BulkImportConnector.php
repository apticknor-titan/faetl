<?php
namespace App\Connectors;

use App\Context;
use Httpful\Request;
use App\Models\Record;
use League\Csv\Reader;
use Webpatser\Uuid\Uuid;
use App\Models\JobStatus;

use League\Csv\Statement;
use Illuminate\Support\Arr;
use App\Actions\ActionPayload;
use App\Exceptions\RecordsAreNotLoaded;
use Illuminate\Support\Facades\Storage;
use App\Actions\Input\DebounceWebhookAction;
use App\Actions\Input\TransformRecordAction;
use App\Connectors\MagentoTwoConnector;

class BulkImportConnector extends AbstractConnector
{
    /**
     * A string containing the next solution to call when the webhook is processed
     *
     * @var string
     */
    public $next;

    /**
     * Process Input
     *
     * @return string[]
     */
    public function getRecords()
    {
        // 1 - Resolve data
        $request = request()->all();

        // 2 - Verify records exist
        if (isset($request['records']) && count($request['records']) > 0){
            return $request['records'];
        }

        // 3 - No records were found in the payload so we should throw an error
        return false;
    }

    /**
     * Receive the records from the HTTP request and queue them for transformation
     *
     * @return string[]
     */
    public function receiveRecords($events){
        // 0 - Init/Preload
        $context = resolve('context');
        $jobs = [];

        // 1 - Open the connector
        if (!$result = $this->getRecords()){
            return [];
        }


        if($result[0]['type'] == 'image') {
            $this->pushImages($result);
        }
        
        // $inputId = Context::generateSessionId();

        // 2 - Create a job for each record
        foreach ($result as $record){
            // if($record['type'] !== 'image') {
                // Generate a new input ID for each record
                $inputId = Context::generateSessionId();
            // }
            
            // 2a - Create a Record
            $payloadRecord = Record::make([
                'input_id' => $inputId,
                'data' => json_encode($record),
                'project' => $context->meta->slug,
                'solution' => self::eventForType($events, $record['type'])
            ]);

            // 2b - Prepare to dispatch the action
            // 
            $payload = new ActionPayload($payloadRecord);

            // Payload would then return a class with the record class
            // 

            // 2c - Add to the queue
            // This is part of the abstract connector payload
            $jobs[] = $this->queueForTransformation($payload);

        }

        // 3 - Return the job statuses
        foreach ($jobs as $job){
            $returnStatus[] = $job->toApiResponse();
        }

        return $returnStatus;
    }


    public function pushImages($images) {
        $payload = array('records'=> array());
        foreach($images as $image) {
            $image['tag_base'] = isset($image['is_base']) && $image['is_base'] ? true : false;
            $image['tag_small_image'] = isset($image['is_small']) && $image['is_small'] ? true : false;
            $image['tag_thumbnail'] = isset($image['is_thumbnail']) && $image['is_thumbnail'] ? true : false;
            $payload['records'][] = $image;
        }

        
        $result = $this->parseApiResponse(Request::post('http://forceamerica.com.c.7ed5khxnlq7eu.ent.magento.cloud/rest/V1/etl-server/import')
                        ->expectsJson()
                        ->sendsJson()
                        ->body(json_encode($payload))
                        ->send());

        return $result;
    }

    /**
     * Return the event for the type
     * Ex. 'order' => 'wh_map_order'
     *
     * @param string $type
     * @return string
     */
    public static function eventForType($events, $type){
        foreach ($events as $event){
            if (isset($event->$type)){
                return $event->$type;
            }
        }
    }

            /**
     * Parse the response from the API
     *
     * @todo Add better error handling
     * @param  $response
     * @return mixed
     */
    public function parseApiResponse($response){
        if (!isset($response->body)) {
            throw new ApiReturnedAnError($response);
        }

        if (isset($response->body->status) &&
                $response->body->status != 200){
            throw new ApiReturnedAnError($response);
        }

        return $response->body;
    }
}
