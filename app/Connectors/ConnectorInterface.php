<?php
namespace App\Connectors;


interface ConnectorInterface
{
    /**
     * Returns all of the public controls for this connector
     *
     * @return mixed
     */
    public function getControls();

}
