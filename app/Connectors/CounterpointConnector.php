<?php
namespace App\Connectors;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Arr;
use League\Csv\Reader;
use League\Csv\Statement;
use Webpatser\Uuid\Uuid;
use Httpful\Request;
use Httpful\Response;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

use App\Exceptions\RecordsAreNotLoaded;
use App\Events\OutputWorkflowFailed;
use App\Actions\ActionPayload;
use App\Actions\Input\TransformRecordAction;
use App\Actions\Input\DebounceWebhookAction;
use App\Models\Record;
use App\Models\Order;
use App\Models\OrderItem;
use App\Maps\Map;
use App\Traits\ConsumesAnApi;
use App\Actions\Input\BigCommerce\OrderCreatedAction;
use App\Records\TransformRecordHelper;
use App\Exceptions\RetrieveRecordFailed;
use App\Context;



class CounterpointConnector extends AbstractConnector implements ApiConnectorInterface
{
    /**
     * The "ErrorCode" from CP that indicates success.
     */
    const RESPONSE_SUCCESS =  "SUCCESS";

    /**
     * Record not found
     */
    const ERROR_RECORD_NOT_FOUND = "ERROR_RECORD_NOT_FOUND";

    /**
     * Slug for Order endpoint
     */
    const API_ORDER = "order";

    /**
     * Slug for Products endpoint
     */
    const API_PRODUCT = "product";

    /**
     * Item Header
     */
    const ITEM_HEADER = "IM_ITEM";

    /**
     * Inventory Header
     */
    const INVENTORY_HEADER = "IM_INV";

    /**
     * Item Sku Header
     */
    const ITEM_ID = "ITEM_NO";


    /**
     * The header object for the document
     *
     * @var string[]
     */
    private $header;

    /**
     * Return the event for the type
     * Ex. 'order' => 'wh_map_order'
     *
     * @param string $type
     * @return string
     */
    public static function eventForType($events, $type){
        foreach ($events as $event){
            if (isset($event->$type)){
                return $event->$type;
            }
        }
    }

    /**
     * Prepare the orders to be sent to CP
     *
     * @return void
     */
    public function prepare($header){
        // 1 - Bootstrap the project and load the map
        $context = context();
        $this->header = $header;

        // 2 - Load all of the orders created
        // Orders are removed once they are processed.
        $orders = Order::all();
        foreach ($orders as $order){
            // 3 - Resolve order to simple array
            $orderData = $order->toArray();

            // Create a Record
            $record = Record::make([
                'input_id' => $context->sessionId,
                'data' => json_encode($orderData),
                'project' => $context->slug,
                'solution' => $context->solution
            ]);

            // 4 - Transform the order record
            $helper = new TransformRecordHelper($record);
            $orderResult = $helper->handle();

            // 5 - Get and transform the orderItem records
            $orderItems = OrderItem::where('input_id', '=', $order->input_id)->get();

            $orderItemResult = [];
            foreach ($orderItems as $orderItem){
                // Create a Record
                $record = Record::make([
                    'input_id' => $context->sessionId,
                    'data' => json_encode($orderItem->toArray()),
                    'project' => $context->slug,
                    'solution' => $context->solution,
                    'map' => 'orderitem'
                ]);

                // 6 - Transform the order record
                $helper = new TransformRecordHelper($record);
                $orderItemResult[] = $helper->handle();
            }
        }

        $this->sendOrder($orderResult, $orderItemResult);
    }

    /**
     * Create the order in CP from the transformed output.
     *
     * @param mixed[] $orders
     * @return void
     */
    public function sendOrder($order, $orderItems){
        // 1 - Format the orderItems
        foreach ($orderItems as $orderItem){
            $data = $orderItem->toArray();
            $lines[] = $data['PS_DOC_LIN'];
        }

        $document = array_merge((array)$this->header[0], $order->toArray());

        $document = [
            "PS_DOC_HDR" => $document
        ];

        $document['PS_DOC_HDR']['PS_DOC_LIN'] = $lines;

        // 2 - Publish the document
        $response = Request::post($this->base_url . "Document")
                        ->sendsJson()
                        ->expectsJson()
                        ->addHeader('APIKey', $this->APIKey)
                        ->authenticateWith($this->username, $this->password)
                        ->body(json_encode($document))
                        ->send();

        // Check Response
        if (!isset($response->body->ErrorCode) && $response->body->ErrorCode != "SUCCESS"){
            // Report an error
            event(new OutputWorkflowFailed($order, $response));
            return;
        }

        // 3 - If a document ID was returned, the order was created successfully
        // We can remove all of the models and records for this session
        $this->clean($order, $orderItems);
    }

    /**
     * Get all ecommerce categories from Counterpoint
     *
     * @return void
     */
    public function loadCategories(){
        // 1 - Get all the categories
        $categories = $this->getEcCategories();
        foreach ($categories as $key => $category){
            // 2 - Iterate through each category, get the image, then queue for transformation
            // 2a - If image exists, get it.
            if (isset($category->IMG_FILE) && !empty($category->IMG_FILE)){
                $category->IMAGE =
                [
                    'filename' => $category->IMG_FILE,
                    'data' => $this->getCategoryImage($category->IMG_FILE)
                ];
            }
        }

        // 3 - Pass raw categories to the next method
        return $categories;
    }

    /**
     * Queue each category for mapping
     *
     * @param string[] $categories
     * @return void
     */
    public function queueCategoriesForMapping($events, $categories){
        foreach ($categories as $key => $category){
            // 1 - Generate an input_id to associate images and categories
            $input_id = utf8_encode(Uuid::generate());
            $context = context();

            // 2 - Build transform record
            $payloadRecord = Record::make([
                'input_id' => $input_id,
                'data' => json_encode($category),
                'project' => $context->meta->slug,
                'solution' => self::eventForType($events, 'create')
            ]);

            // 1b - Prepare to dispatch the action
            $payload = new ActionPayload($payloadRecord);

            // 1c - Dispatch the event
            $this->queueForTransformation($payload);
        }
    }

    /**
     * Retrieve all of the products
     *
     * @return string[]
     */
    public function loadProducts($sku = null){
        // 0 - Preload/Init
        $context = context();

        // 1 - Make request to CP API
        // TODO Connect this value to the agent to run a delta from last run
        Log::debug('Loading products...');
        $products = $this->getProducts();

        // $products = array_slice($products, 0, 100);

        // 2 - Load the product categories
        Log::debug('Loading eCommerce categories...');
        $categories = $this->getEcCategories();

        // 3 - Add categories to products
        Log::debug('Creating Product <> Category associations...');
        $result = $this->combineProductsAndCategories($products, $categories);

        // 2 - Parse results
        return $result;
    }

    public function combineProductsAndCategories($products, $categories){
        // Loop through each category
        // Get each category and create a hierarchy
        // Then find each product in each category
        // Make a list of categories as an array for each product
        // APPAREL,EQUIPMENT,MISC

        $categoriesById = [];

        foreach ($categories as $category){
            $categoriesById[$category->CATEG_ID] = (array)$category;

            foreach ($category->EC_CATEG_ITEM as $item){
                foreach ($products as $key => $product){
                    if ($product->ITEM_NO == $item->ITEM_NO){
                        Log::debug('Product ' . $key . ' is in category ' . $category->CATEG_ID);
                        $products[$key]->categories[] = $category->CATEG_ID;
                    }
                }
            }
        }

        return $products;
    }

    /**
     * Get product details from the API for every product.
     *
     * @param string[] $products
     * @return string[]
     */
    public function loadProductDetails($products){
        // 1 - Loop through and retrieve details for each product
        foreach ($products as $key => $item){
            // 2 - Retrieve the details one at a time.
            Log::debug('Getting product details for '. $item->ITEM_NO);
            $itemData = $this->getProduct($item->ITEM_NO);

            // 3 - Merge both data sets
            $products[$key] = array_merge((array)$item, (array)$itemData);
        }

        // Pass the products with details onto the next method.
        return $products;
    }

    /**
     * Get the product images for each product
     *
     * @param string[] $products
     * @return string[]
     */
    public function loadProductImages($products){
        // 1 - Loop through each product and use the product_id to retrieve the files.
        foreach ($products as $key => $item){
            Log::debug('Getting product images for '. $item['ITEM_NO']);
            $images = $this->getProductImages($item['ITEM_NO']);

            // No images found, skip this product
            if (is_null($images)){
                continue;
            }

            $products[$key]["IMAGES"] = [];

            foreach ($images as $image){
                $products[$key]["IMAGES"][] =
                    [
                        'filename' => $image,
                        'data' => $this->getImage($image)
                    ];
            }

        }

        // 2 - Pass off to the next method
        return $products;
    }


    /**
     * Queue all categories and products for transformation
     *
     * @param string[] $combined
     * @return void
     */
    public function processProducts($next, $products){
        // 0 - Preload
        $context = context();

        // 1 - Loop through each product and generate a transformation record for each
        foreach ($products as $key => $item){
            Log::debug('Queueing Product '. $item['ITEM_NO'] . ' for transformation.');
            // Create a Record
            $record = Record::make([
                'input_id' => Context::generateSessionId(),
                'data' => json_encode($item),
                'project' => $context->slug,
                'solution' => $context->solution
            ]);

            // 1b - Prepare to dispatch the action
            $payload = new ActionPayload($record);

            // 1c - Dispatch the event
            $this->queueForTransformation($payload);
        }
    }

    /**
     * Get the image data and return it as a Base64 encoded file
     *
     * @param string $filename
     * @return string[]
     */
    public function loadImageData($filename){
        // 1 - Make request to CP API for the image data
        $rawResponse = Request::get($this->base_url . "Item/Images/" . $filename)
            ->addHeader('APIKey', $this->APIKey)
            ->authenticateWith($this->username, $this->password)
            ->send();

        // 2 - Validate and parse response
        // TODO Add validation for images
        $imageData = $rawResponse->body;

        // 3 - Return the encoded image
        return base64_encode($imageData);

    }

    /**
     * Get one item from Counterpoint
     *
     * @param mixed $product_id
     * @return string
     */
    public function getOneItem($product_id){
        $response = Request::get($this->base_url . "Item/" . $product_id)
            ->sendsJson()
            ->expectsJson()
            ->addHeader('APIKey', $this->APIKey)
            ->authenticateWith($this->username, $this->password)
            ->send();

        $body = $this->parseResponse($response);

        if (!isset($body->IM_ITEM)){
            throw new RetrieveRecordFailed($response->body->Message ?? "");
        }

        return $body->IM_ITEM;


    }

    /**
     * Parse the response for an error, otherwise return the body
     *
     * @param Response $response
     * @return string
     */
    public function parseResponse($response, $optional = false){
        if (!isset($response->body->ErrorCode)
                || $response->body->ErrorCode != self::RESPONSE_SUCCESS){

            // TODO: Handle the error better
            if (!$optional){
                throw new RetrieveRecordFailed($response->body->Message ?? "");
            }

            return null;
        }

        return $response->body;
    }

    /**
     * Load the customers from Counterpoint
     *
     * @return string[]
     */
    public function loadCustomers(){
        // 1 - Preload
        $context = context();

        // 2 - Setup query
        // TODO Refactor to use the delta date from agent
        $query = [
            'StartDate' => '2019-10-01T00:00:00'
        ];

        // 3 - Run query
        $customers = $this->getCustomers($query);

        // 4 - Pass off to next method
        return $customers;
    }

    /**
     * Receive the loaded customers from CP and transform them
     *
     * @param string[] $previous
     * @return string[]
     */
    public function transformCustomers($next = null, $previous = null){
        // 0 - Preload/Init
        $context = context();

        // 1 - Loop through every customer and transform it
        foreach ($previous as $customer){
            // Reject customers that have missing required info
            // TODO - Refactor this to be in the definition file some how
            if (!isset($customer->FST_NAM) || empty($customer->FST_NAM)){
                continue;
            }
            if (!isset($customer->LST_NAM) || empty($customer->LST_NAM)){
                continue;
            }

            // Use one ID for this transformation
            $input_id = Context::generateSessionId();

            // Create a Record
            $record = Record::make([
                'input_id' => $input_id,
                'data' => json_encode($customer),
                'project' => $context->slug,
                'solution' => $context->solution
            ]);

            // Transform the record
            $helper = new TransformRecordHelper($record);
            $transformed = $helper->handle();
            $transformed->save();
        }

        // 2 - End of Solution
    }


    public $endpoints = [
        "example" => [
            "find" => "v3/catalog/products?{{search_query}}",
            "create" => "v3/catalog/products",
            "read-all" => "v3/catalog/products",
            "read" => "v3/catalog/products/{{product_id}}",
            "update" => "v3/catalog/products/{{product_id}}?include_fields={{include_fields}}",
            "delete" => "v3/catalog/products/{{product_id}}",
        ],
        "category" => [
            "read-all" => "ECCategories?{{search_query}}",
        ],
        "product" => [
            "read-all" => "Inventory/EC?{{search_query}}",
            "read" => "Item/{{item_no}}",
        ],
        "productimage" => [
            "read-all" => "Item/{{item_no}}/Images",
        ],
        "image" => [
            "read" => "Item/Images/{{filename}}",
        ],
        "customer" => [
            "read" => "",
            "read-all" => "Customers/EC?{{search_query}}",
        ]
    ];

    /**
     * Build the URL from the endpoint
     *
     * @param string $endpoint
     * @param string[]|mixed $parameters
     * @return string
     */
    public function makeUrl($endpoint, $parameters = []){
        $entries = array_dot($this->endpoints);
        return $this->parse($this->base_url . $entries[$endpoint], $parameters);
    }

    /**
     * Parse the response from the API
     *
     * @todo Add better error handling
     * @param  $response
     * @return mixed
     */
    public function parseApiResponse($response, $optional = false){
        if (!isset($response->body->ErrorCode)
                || $response->body->ErrorCode != self::RESPONSE_SUCCESS){

            // TODO: Handle the error better
            if (!$optional){
                throw new RetrieveRecordFailed($response->body->Message ?? "");
            }

            return null;
        }

        return $response->body;
    }

    /**
     * Return the categories from the ECCategories Endpoint
     *
     * @return string[]
     */
    public function getEcCategories(){
        $url = $this->makeUrl("category.read-all");

        $response = $this->parseApiResponse(Request::get($url)
                    ->expectsJson()
                    ->sendsJson()
                    ->addHeader('APIKey', $this->APIKey)
                    ->authenticateWith($this->username, $this->password)
                    ->send());
        return $response->EC_CATEG;
    }

    /**
     * Return the image file data from the category
     *
     * @return string[]
     */
    public function getCategoryImage($filename){
        // 1 - Make request to CP API for the image data
        $url = $this->makeUrl("image.read", ['filename' => $filename]);

        // 2 - Validate and parse response
        // TODO Add validation for images
        $imageData = Request::get($url)
                    ->addHeader('APIKey', $this->APIKey)
                    ->authenticateWith($this->username, $this->password)
                    ->send();

        // 3 - Return the encoded image
        return base64_encode($imageData);
    }

    /**
     * Get all products
     *
     * @return string[]
     */
    public function getProducts(){
        $url = $this->makeUrl("product.read-all", ['search_query' => '']);

        $response = $this->parseApiResponse(Request::get($url)
                    ->expectsJson()
                    ->sendsJson()
                    ->addHeader('APIKey', $this->APIKey)
                    ->authenticateWith($this->username, $this->password)
                    ->send());

        return $response->IM_INV;
    }

    /**
     * Get a single product by SKU
     *
     * @param string $item_no
     * @return string[]
     */
    public function getProduct($item_no){
        $url = $this->makeUrl("product.read", ['item_no' => $item_no]);

        $response = $this->parseApiResponse(Request::get($url)
                    ->expectsJson()
                    ->sendsJson()
                    ->addHeader('APIKey', $this->APIKey)
                    ->authenticateWith($this->username, $this->password)
                    ->send());

        return $response->IM_ITEM;
    }

    /**
     * Get a list of images from the product model
     *
     * @return string[]|null
     */
    public function getProductImages($item_no){
        $url = $this->makeUrl("productimage.read-all", ['item_no' => $item_no]);

        $response = $this->parseResponse(Request::get($url)
                        ->expectsJson()
                        ->addHeader('APIKey', $this->APIKey)
                        ->authenticateWith($this->username, $this->password)
                        ->send(), true); // Response is optional.

        // 2 - Validate and parse response
        if (is_null($response)){
            return null;
        }

        // Return the filenames
        return $response->IMAGE_FILENAMES;
    }

    public function getProductCategories(){


    }

    public function getImages(){

    }

    public function getImage($filename){
        // 1- Build URL
        $url = $this->makeUrl("image.read", ['filename' => $filename]);

        // 2 - Receive raw image data
        $rawResponse = Request::get($url)
                        ->addHeader('APIKey', $this->APIKey)
                        ->authenticateWith($this->username, $this->password)
                        ->send();

        // 2 - Validate and parse response
        // TODO Add validation for images
        $imageData = $rawResponse->body;

        // 3 - Return the encoded image
        return base64_encode($imageData);

    }

    /**
     * Get all products
     *
     * @return string[]
     */
    public function getCustomers($searchQuery){
        // Init various query values
        $customers = [];
        $page = 1;
        $rows = 10;
        $more = true;

        // Continuous loop until there are no more records being returned
        while ($more){
            Log::debug('Getting customers. Page: ' . $page);
            $searchQuery = array_merge($searchQuery, [
                'Page' => $page,
                'Rows' => $rows
            ]);

            // Generate URL
            $url = $this->makeUrl("customer.read-all", ['search_query' => http_build_query($searchQuery)]);

            // Make API Request with search query
            $response = $this->parseApiResponse(Request::get($url)
                    ->expectsJson()
                    ->sendsJson()
                    ->addHeader('APIKey', $this->APIKey)
                    ->authenticateWith($this->username, $this->password)
                    ->send());

            // Combine existing records with retrieved records
            $customers = array_merge($customers, $response->Customers);

            // If there are no more customers in this response, end the loop
            if (count($response->Customers) === 0){
                $more = false;
            } else {
                $page = $page + 1;
            }
        }

        return $customers;
    }

    public function getCustomer(){

    }
    public function getOrders(){

    }
    public function getOrder(){

    }
    public function createOrder(){

    }




}
