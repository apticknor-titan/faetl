<?php
namespace App\Connectors;

use Illuminate\Support\Facades\Storage;
use League\Csv\Reader;
use League\Csv\Statement;
use Webpatser\Uuid\Uuid;

use App\Exceptions\RecordsAreNotLoaded;
use App\Actions\ActionPayload;
use App\Actions\Input\TransformRecordAction;
use App\Models\Record;



class CsvConnector extends AbstractConnector
{
    /**
     * The CSV Headers
     *
     * @var $headers
     */
    protected $headers = null;

    /**
     * Open the CSV File
     *
     * @return CsvConnector
     */
    public function open()
    {
        // 1 - Open the file path located in config
        $csv = Reader::createFromPath($this->file, 'r');

        // 2 - Get the header and unique-ify duplicates.
        $headers = $csv->fetchOne();

        // 3 - Handle Duplicate Column Names
        $headerCounts = [];
        foreach ($headers as $key => $header){
            // Check if the header has already been stored
            if (isset($headerCounts[$header])){
                // Header has already been counted
                // Increment the value and then rename the header in the list
                $headerCounts[$header]++;
                $headers[$key] = $header . "_" . $headerCounts[$header];
            } else {
                // Header has not been counted yet.
                // We start at 1 so that the duplicate children are _2, _3, _N
                $headerCounts[$header] = 1;
            }
        }

        // Garbage Collection
        unset($headerCounts);

        $this->resource = $csv;
        $this->headers = $headers;

        return $this;
    }

    /**
     * Load records from the CSV
     *
     * @control
     * @return mixed
     */
    public function getRecords(){
        // 1 - Open the CSV file
        $this->open();

        // 2 - Process the records
        $stmt = new Statement();
        $this->resource->setHeaderOffset(0);
        $this->records = $stmt->process($this->resource, $this->headers);

        return $this->records;
    }

    /**
     * Prepare a transformation event for each record
     *
     * @control
     * @return void
     */
    public function mapProducts(){
        // 1 - Make sure the records exist
        if (empty($this->records)){
            throw new RecordsAreNotLoaded();
        }

        $context = resolve('context');

        // 2 - Ingest each line item as a product record
        foreach ($this->records as $record){
            // We have to include the map to be used for this solution. But the conncetor doesn't know about the soluton here
            // Perhaps the solution should be passed through to the connector?
            $payloadRecord = Record::create([
                'input_id' => utf8_encode(Uuid::generate()),
                'data' => json_encode($record),
                'project' => $context->meta->slug,
                'solution' => $context->getRunningSolution()
            ]);

            // 3 - Prepare and dispatch the transformation action
            $payload = new ActionPayload($payloadRecord);
            $this->queueForTransformation($payload);
        }

    }

}
