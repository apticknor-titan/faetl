<?php
namespace App\Connectors;

use Webpatser\Uuid\Uuid;

use App\Actions\RunSolutionAction;
use App\Actions\ActionPayload;
use App\Models\Record;


class InternalConnector extends AbstractConnector
{
    /**
     * Queue a job to run the specified solution path
     *
     * @param string $solutionPath
     * @return void
     */
    public function dispatchSolution($solutionPath){
        dd("Next:", $this->next);
        $record = Record::create([
            'input_id' => utf8_encode(Uuid::generate()),
            'project' => $project,
            'solution' => $solution
        ]);

        $payload = new ActionPayload($record);
        dispatch(new RunSolutionAction($payload));
    }
}
