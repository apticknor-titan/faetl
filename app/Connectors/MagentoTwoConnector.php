<?php
namespace App\Connectors;

use Httpful\Request;

use App\Models\Image;
use App\Models\Order;
use App\Models\Price;
use App\Models\Product;
use App\Models\Customer;
use App\Models\Address;
use App\Models\Inventory;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use App\Exceptions\ApiReturnedAnError;

class MagentoTwoConnector extends AbstractConnector
{

    /**
     * Create or Update a product in M2
     *
     * @return void
     */
    public function putProduct($previous, $input_id){
        // 0 - Preload
        $context = context();
        $processed = [];

        // 1
        // TODO refactor the output ID slug to be the connector slug
        $products = Product::where('input_id', $input_id)->get();

        // 2 - Loop through each product
        foreach ($products as $key => $product){
            Log::debug('Working on product: ' . $product->input_id);
            $product->type = "product";

            // 2c - Collect the results;
            $result[] = $product->toArray();
            $processed[] = $product->input_id;
        }

        // 3 - Format an output array
        $productRecords = [
            'records' => $result
        ];

        // 4 - Send request
        $this->createProduct($productRecords);

        // 5 - Clean records
        foreach ($processed as $record){
            $this->clean($record)
                ->clean(Product::class)
                ->clean();
        }

        return;

    }

    /**
     * Add image to product in Magento two
     *
     * @return void
     */
    public function putImage($previous, $input_id){
        // 0 - Preload
        $context = context();
        $processed = [];

        // 1
        // TODO refactor the output ID slug to be the connector slug
        $images = Image::where('input_id', $input_id)->get();

        if(empty($images)) {
            return;
        }

        // 2 - Loop through each product
        foreach ($images as $key => $image){
            Log::debug('Working on image: ' . $image->input_id);
            $image->type = "image";

            // 2c - Collect the results;
            $result[] = $image->toArray();
            $processed[] = $image->input_id;
        }

        // 3 - Format an output array
        $imageRecords = [
            'records' => $result
        ];

        // 4 - Send request
        $this->createImage($imageRecords);

        // Remove the input immediately so it no longer exists for the next round
        // Image::where('input_id', $input_id)->delete();

        // 5 - Clean records
        foreach ($processed as $record){
            $this->clean($record)
                ->clean(Image::class)
                ->clean();
        }

        return;
    }

    /**
     * Update Inventory
     *
     * @return void
     */
    public function putInventory($previous, $input_id){
        // 0 - Preload
        $context = context();
        $processed = [];

        // 1
        // TODO refactor the output ID slug to be the connector slug
        // $inventory = Inventory::where('output_id', 'magento_two')->get();
        $inventory = Inventory::where('input_id', $input_id)->get();

        // 2 - Loop through each inventory update
        foreach ($inventory as $key => $inventory){
            Log::debug('Working on inventory update: ' . $inventory->input_id);
            $inventory->type = "inventory";

            // 2c - Collect the results;
            $result[] = $inventory->toArray();
            $processed[] = $inventory->input_id;
        }

        // 3 - Format an output array
        $inventoryRecords = [
            'records' => $result
        ];

        // 4 - Send request
        $this->updateInventory($inventoryRecords);

        // 5 - Clean records
        foreach ($processed as $record){
            $this->clean($record)
                ->clean(Inventory::class)
                ->clean();
        }

        return;
    }

    /**
     * Update Customer
     *
     * @return void
     */
    public function putCustomer($previous, $input_id){
        // 0 - Preload
        $context = context();
        $processed = [];

        // 1
        // TODO refactor the output ID slug to be the connector slug
        $customer = Customer::where('input_id', $input_id)->get();

        // 2 - Loop through each customer update
        foreach ($customer as $key => $customer){
            Log::debug('Working on customer update: ' . $customer->input_id);
            $customer->type = "customer";

            // 2c - Collect the results;
            $result[] = $customer->toArray();
            $processed[] = $customer->input_id;
        }

        // 3 - Format an output array
        $customerRecords = [
            'records' => $result
        ];

        // 4 - Send request
        $this->updateCustomer($customerRecords);

        // 5 - Clean records
        foreach ($processed as $record){
            $this->clean($record)
                ->clean(Customer::class)
                ->clean();
        }

        return;
    }

    /**
     * Update Customer
     *
     * @return void
     */
    public function putAddress($previous, $input_id){
        // 0 - Preload
        $context = context();
        $processed = [];

        // 1
        // TODO refactor the output ID slug to be the connector slug
        $address = Address::where('input_id', $input_id)->get();

        // 2 - Loop through each customer update
        foreach ($address as $key => $address){
            Log::debug('Working on address update: ' . $address->input_id);
            $address->type = "address";

            // 2c - Collect the results;
            $result[] = $address->toArray();
            $processed[] = $address->input_id;
        }

        // 3 - Format an output array
        $addressRecords = [
            'records' => $result
        ];

        // 4 - Send request
        $this->updateAddress($addressRecords);

        // 5 - Clean records
        foreach ($processed as $record){
            $this->clean($record)
                ->clean(Customer::class)
                ->clean();
        }

        return;
    }

    /**
     * Update Order
     *
     * @return void
     */
    public function putOrder($previous, $input_id){
        // 0 - Preload
        $context = context();
        $processed = [];

        // 1
        // TODO refactor the output ID slug to be the connector slug
        $order = Order::where('input_id', $input_id)->get();

        // 2 - Loop through each order update
        foreach ($order as $key => $order){
            Log::debug('Working on order update: ' . $order->input_id);
            $order->type = "order";

            // 2c - Collect the results;
            $result[] = $order->toArray();
            $processed[] = $order->input_id;
        }

        // 3 - Format an output array
        $orderRecords = [
            'records' => $result
        ];

        // 4 - Send request
        $this->updateOrder($orderRecords);

        // 5 - Clean records
        foreach ($processed as $record){
            $this->clean($record)
                ->clean(Order::class)
                ->clean();
        }

        return;
    }

    /**
     * Update Order
     *
     * @return void
     */
    public function putPrice($previous, $input_id){
        // 0 - Preload
        $context = context();
        $processed = [];

        // 1
        // TODO refactor the output ID slug to be the connector slug
        $price = Price::where('input_id', $input_id)->get();

        // 2 - Loop through each order update
        foreach ($price as $key => $price){
            Log::debug('Working on price update: ' . $price->input_id);
            $price->type = "price";

            // 2c - Collect the results;
            $result[] = $price->toArray();
            $processed[] = $price->input_id;
        }

        // 3 - Format an output array
        $priceRecords = [
            'records' => $result
        ];

        // 4 - Send request
        $this->updatePrice($priceRecords);

        // 5 - Clean records
        foreach ($processed as $record){
            $this->clean($record)
                ->clean(Price::class)
                ->clean();
        }

        return;
    }



    /**
     * A list of endpoints for the M2 Connector Extension
     *
     * @var array
     */
    public $endpoints = [
        "base_url" => "{{base_url}}",
        "customer" => [
            "find" => "",
            "create" => "",
            "read-all" => "",
            "read" => "",
            "update" => "",
            "delete" => "",
        ],
        "address" => [
            "find" => "",
            "create" => "",
            "read-all" => "",
            "read" => "",
            "update" => "",
            "delete" => "",
        ],
        "product" => [
            "find" => "",
            "create" => "",
            "read-all" => "",
            "read" => "",
            "update" => "",
            "delete" => "",
        ],
        "productimage" => [
            "find" => "",
            "create" => "",
            "read-all" => "",
            "read" => "",
            "update" => "",
            "delete" => "",
        ],
        "inventory" => [
            "find" => "",
            "create" => "",
            "read-all" => "",
            "read" => "",
            "update" => "",
            "delete" => "",
        ],
        "category" => [
            "find" => "",
            "create" => "",
            "read-all" => "",
            "read" => "",
            "update" => "",
            "delete" => "",
        ],
        "order" => [
            "find" => "",
            "create" => "",
            "read-all" => "",
            "read" => "",
            "update" => "",
            "delete" => "",
        ],
        "price" => [
            "find" => "",
            "create" => "",
            "read-all" => "",
            "read" => "",
            "update" => "",
            "delete" => "",
        ],
    ];

    /**
     * Build the URL from the endpoint
     *
     * @param string $endpoint
     * @param string[]|mixed $parameters
     * @return string
     */
    public function makeUrl($endpoint, $parameters = []){
        if (!isset($parameters['base_url'])) {
            $baseUrl = "base_url_" . env('SERVICE_ENVIRONMENT', 'local');
            $parameters['base_url'] = $this->$baseUrl;
        }
        $entries = Arr::dot($this->endpoints);
        return $this->parse($entries['base_url'] . $entries[$endpoint], $parameters);
    }

        /**
     * Parse the response from the API
     *
     * @todo Add better error handling
     * @param  $response
     * @return mixed
     */
    public function parseApiResponse($response){
        if (!isset($response->body)) {
            throw new ApiReturnedAnError($response);
        }

        if (isset($response->body->status) &&
                $response->body->status != 200){
            throw new ApiReturnedAnError($response);
        }

        return $response->body;
    }

    /**
     * Create a product in Magento 2
     *
     * @param string[] $product
     * @return void
     */
    public function createProduct($product){
        $url = $this->makeUrl("product.create");

        $this->removeInternalAttributes($product);

        $payload = json_encode($product);

        return $this->parseApiResponse(Request::post($url)
                    ->expectsJson()
                    ->sendsJson()
                    ->body($payload)
                    ->send());
    }

    /**
     * Create an image in Magento 2
     *
     * @param string[] $image
     * @return void
     */
    public function createImage($image){
        $url = $this->makeUrl("productimage.create");

        Log::debug('Sending Image request to: ' . $url);

        $this->removeInternalAttributes($image);

        $payload = json_encode($image);

        Log::debug('Sending Image payload: ' . $payload);
        
        return [];

        // $result = $this->parseApiResponse(Request::post($url)
        //             ->expectsJson()
        //             ->sendsJson()
        //             ->body($payload)
        //             ->send());

        // return $result;
    }

    /**
     * Update inventory for a source in Magento 2
     *
     * @param string[] $inventory
     * @return void
     */
    public function updateInventory($inventory){
        $url = $this->makeUrl("inventory.create");

        $this->removeInternalAttributes($inventory);

        $payload = json_encode($inventory);

        return $this->parseApiResponse(Request::post($url)
                    ->expectsJson()
                    ->sendsJson()
                    ->body($payload)
                    ->send());
    }

    /**
     * Create or update a Customer
     *
     * @param string[] $customer
     * @return void
     */
    public function updateCustomer($customer){
        $url = $this->makeUrl("customer.create");

        Log::debug('Sending Customer request to: ' . $url);

    
        $this->removeInternalAttributes($customer);

        $payload = json_encode($customer);

        Log::debug('Sending Customer payload: ' . $payload);

        return $this->parseApiResponse(Request::post($url)
                    ->expectsJson()
                    ->sendsJson()
                    ->body($payload)
                    ->send());
    }

    /**
     * Create or update a Customer
     *
     * @param string[] $customer
     * @return void
     */
    public function updateAddress($address){
        $url = $this->makeUrl("address.create");

        Log::debug('Sending Address request to: ' . $url);

    
        $this->removeInternalAttributes($address);

        $payload = json_encode($address);

        Log::debug('Address payload to: ' . $payload);

        return $this->parseApiResponse(Request::post($url)
                    ->expectsJson()
                    ->sendsJson()
                    ->body($payload)
                    ->send());
    }

    /**
     * Create or update an Order
     *
     * @param string[] $order
     * @return void
     */
    public function updateOrder($order){
        $url = $this->makeUrl("order.create");

        $this->removeInternalAttributes($order);

        $payload = json_encode($order);

        return $this->parseApiResponse(Request::post($url)
                    ->expectsJson()
                    ->sendsJson()
                    ->body($payload)
                    ->send());
    }

    /**
     * Create or update a Price
     *
     * @param string[] $price
     * @return void
     */
    public function updatePrice($price){
        $url = $this->makeUrl("price.create");

        $this->removeInternalAttributes($price);

        Log::debug('Sending price request to: ' . $url);

        $payload = json_encode($price);

        Log::debug('Price payload request to: ' . $payload);

        return $this->parseApiResponse(Request::post($url)
                    ->expectsJson()
                    ->sendsJson()
                    ->body($payload)
                    ->send());
    }

}
