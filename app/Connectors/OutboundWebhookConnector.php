<?php
namespace App\Connectors;

use App\Context;
use Httpful\Request;
use App\Models\Record;
use League\Csv\Reader;
use Webpatser\Uuid\Uuid;
use App\Models\JobStatus;

use League\Csv\Statement;
use Illuminate\Support\Arr;
use App\Actions\ActionPayload;
use App\Exceptions\RecordsAreNotLoaded;
use Illuminate\Support\Facades\Storage;
use App\Actions\Input\DebounceWebhookAction;
use App\Actions\Input\TransformRecordAction;


class OutboundWebhookConnector extends AbstractConnector
{
    /**
     * A string containing the next solution to call when the webhook is processed
     *
     * @var string
     */
    public $next;

    /**
     * Process Input
     *
     * @return string[]
     */
    public function getRecords()
    {
        // 1 - Resolve data
        $request = request()->all();

        // 2 - Verify records exist
        if (isset($request['records']) && count($request['records']) > 0){
            return $request['records'];
        }

        // 3 - No records were found in the payload so we should throw an error
        return false;
    }

    /**
     * Receive the records from the HTTP request and queue them for transformation
     *
     * @return string[]
     */
    public function receiveRecords($events){
        // 0 - Init/Preload
        $context = resolve('context');
        $jobs = [];

        // 1 - Open the connector
        if (!$result = $this->getRecords()){
            return [];
        }

        // 2 - Create a job for each record
        foreach ($result as $record){
            // Generate a new input ID for each record
            $inputId = Context::generateSessionId();

            // 2a - Create a Record
            $payloadRecord = Record::make([
                'input_id' => $inputId,
                'data' => json_encode($record),
                'project' => $context->meta->slug,
                'solution' => self::eventForType($events, $record['type'])
            ]);

            // 2b - Prepare to dispatch the action
            $payload = new ActionPayload($payloadRecord);

            // 2c - Add to the queue
            $jobs[] = $this->queueForTransformation($payload);

        }

        // 3 - Return the job statuses
        foreach ($jobs as $job){
            $returnStatus[] = $job->toApiResponse();
        }

        return $returnStatus;
    }

    public function postEvent($events, $inputId = null){

    }

    /**
     * Return the event for the type
     * Ex. 'order' => 'wh_map_order'
     *
     * @param string $type
     * @return string
     */
    public static function eventForType($events, $type){
        foreach ($events as $event){
            if (isset($event->$type)){
                return $event->$type;
            }
        }
    }


}
