<?php
namespace App\Connectors;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Arr;
use League\Csv\Reader;
use League\Csv\Statement;
use Webpatser\Uuid\Uuid;

use App\Exceptions\RecordsAreNotLoaded;
use App\Actions\ActionPayload;
use App\Actions\Input\TransformRecordAction;
use App\Models\Record;



class PassiveApiConnector extends AbstractConnector
{
    /**
     * Wait for input
     *
     * @return PassiveConnector
     */
    public function open()
    {
        // 1 - Read the record payload from request data
        $this->resource = request()->get('records');

        return $this;
    }

    /**
     * Load records from the resource
     *
     * @control
     * @return mixed
     */
    public function getRecords(){
        // 1 - Open/Load the record payload
        $this->open();

        // 2 - Simple copy. No payload transformation required
        $this->records = $this->resource;

        return $this->records;
    }



    /**
     * Prepare a transformation event for each record
     *
     * @control
     * @return void
     */
    public function map(){
        // 1 - Make sure the records exist
        if (empty($this->records)){
            throw new RecordsAreNotLoaded();
        }

        $context = resolve('context');

        // 2 - Ingest each line item as a generic record
        foreach ($this->records as $record){
            // Wrap the record in a data element so that the map can be dynamic.
            $data = [
                'data' => $record
            ];

            $payloadRecord = Record::create([
                'input_id' => utf8_encode(Uuid::generate()),
                'data' => json_encode($data),
                'project' => $context->meta->slug,
                'solution' => $context->getRunningSolution()
            ]);

            // 3 - Prepare and dispatch the transformation action
            $payload = new ActionPayload($payloadRecord);
            $this->queueForTransformation($payload);
        }

    }

}
