<?php
namespace App\Connectors;

use Illuminate\Support\Facades\Storage;
use League\Csv\Reader;
use League\Csv\Statement;

use App\Context;

class SampleConnector extends AbstractConnector
{
    /**
     * Solution Context
     *
     * @var $context
     */
    protected $context;

    /**
     * Connector State
     *
     * @var $resource
     */
    protected $resource = null;

    /**
     * The ingested Records
     *
     * @var $records
     */
    protected $records;

    /**
     * Create a new instance of ConnectorInterface with Context
     *
     * @param Context $context
     * @return void
     */
    public function __construct($context){
        $this->setContext($context);
    }

    /**
     * Context setter
     *
     * @param Context $context
     * @return void
     */
    public function setContext($context){
        $this->context = $context;
    }

    /**
     * Context getter
     *
     * @return Context
     */
    public function getContext(){
        return $this->context;
    }

    public function open()
    {
        // If resource is already opened, no need to open it again
        if ($this->isOpen()){
            return $this;
        }

        // 1 - Open the file path located in config
        $this->resource = Reader::createFromPath($this->context->connector->path, 'r');

        // 2 - Get the header and unique-ify duplicates.
        $headers = $this->resource->fetchOne();

        // 3 - Handle Duplicate Column Names
        $headerCounts = [];
        foreach ($headers as $key => $header){
            // Check if the header has already been stored
            if (isset($headerCounts[$header])){
                // Header has already been counted
                // Increment the value and then rename the header in the list
                $headerCounts[$header]++;
                $headers[$key] = $header . "_" . $headerCounts[$header];
            } else {
                // Header has not been counted yet.
                // We start at 1 so that the duplicate children are _2, _3, _N
                $headerCounts[$header] = 1;
            }
        }

        // Garbage Collection
        unset($headerCounts);

        // 4 - Process records
        $stmt = new Statement();
        $this->records = $stmt->process($this->resource, $headers);

        return $this;
    }

    public function getRecords(){
        return $this->records;
    }

    /**
     * Returns the Record Iterator for this connection
     *
     * @return RecordIteratorIterface
     */
    public function records(){
        if (!$this->resource){
            $this->open();
        }

        return $this->records;
    }

    protected function isOpen(){
        if (empty($this->resource)){
            return false;
        }

        return true;
    }


}
