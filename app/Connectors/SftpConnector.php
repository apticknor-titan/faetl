<?php
namespace App\Connectors;

use Illuminate\Support\Facades\Storage;
use League\Flysystem\Filesystem;
use League\Flysystem\Sftp\SftpAdapter;

use App\Exceptions\ResourceIsNotAvailable;
use App\Exceptions\RemoteFileNotReadable;


class SftpConnector extends AbstractConnector
{
    /**
     * Retrieve a remote file and store it locally
     *
     * @control
     * @param string $remote
     * @param string $local
     * @return mixed
     */
    public function getFile($remote, $local){
        // 1 - Open the SFTP Connection
        $this->open();

        // 2 - Read the file into memory
        $file = $this->downloadFile($remote);

        // 3 - Put the file in the local path
        $this->moveFile($local, $file);

        return $this;
    }

    /**
     * Open the SFTP Connection
     *
     * @return SftpConnector
     */
    public function open()
    {
        // If resource is already opened, no need to open it again
        if ($this->isOpen()){
            return $this;
        }

        $config = [];
        $config['host'] = $this->host;
        $config['username'] = $this->username;
        $config['password'] = $this->password;
        $config['privateKey'] = $this->private_key ?? null;
        $config['cache'] = true;
        $config['useAgent'] = false;

        try {
            $this->resource = new Filesystem(new SftpAdapter($config));
        } catch (\Exception $e) {
            return false;
        }

        return $this;
    }

    /**
     * Download the specified file into a resource.
     *
     * @param string $file
     * @throws ResourceIsNotAvailable
     * @throws RemoteFileNotReadable
     * @return string
     */
    public function downloadFile($file){
        if (!$this->isOpen()){
            throw new ResourceIsNotAvailable();
        }

        if ($this->resource->has($file)){
            return $this->resource->read($file);
        }

        throw new RemoteFileNotReadable();
    }

    /**
     * Put $fileContents into $local as a file.
     *
     * @param mixed $local
     * @param mixed $fileContents
     * @return SftpConnector|bool
     */
    public function moveFile($local, $fileContents){
        if (Storage::put($local, $fileContents)){
            return $this;
        }

        return false;
    }

}
