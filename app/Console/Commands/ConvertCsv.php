<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use League\Csv\Reader;
use League\Csv\Statement;

class ConvertCsv extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'convert:csv';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $output = [];
        $csv = Reader::createFromPath('public/redirects2.csv', 'r');
        $csv->setHeaderOffset(0); //set the CSV header offset

        $stmt = new Statement();

        $records = $stmt->process($csv);
        foreach ($records as $record) {
            // Remove the path from the url;
            $oldPath = parse_url($record['old'])['path'];
            $newPath = parse_url($record['new'])['path'];

            // Check for special characters. If they exist, add a line for regular and decoded

            if (stristr($oldPath, '%')){
                $output[] = sprintf("rewrite ^%s$ %s? permanent;%s", urldecode($oldPath), $newPath, PHP_EOL);
            }

            // Construct new line
            $output[] = sprintf("rewrite ^%s$ %s? permanent;%s", $oldPath, $newPath, PHP_EOL);
        }

        $outputString = join('', $output);
        file_put_contents('public/redirects.txt', $outputString);
    }
}
