<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class MongoTestCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'irishtitan:extract-payload {payload}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Extracts a payload from a redis dump';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $payloadFilename = $this->argument('payload');
        $data = json_decode(Storage::disk('tmp')->get($payloadFilename));

        $record = unserialize($data->data->command);
        dump(($record->payload->resource->data));
    }
}
