<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Context;
use App\Exceptions\FailedToLoadSolutionException;

class RunAgent extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'agent:run {project} {agent}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Runs the specified Agent';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Context $context)
    {
        $this->context = $context;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Init arguments
        $projectSlug = $this->argument('project');
        $agentSlug = $this->argument('agent');

        // Start the project
        /** @var App\Solutions\SolutionInterface $solution */
        if (!$project = $this->context->project($projectSlug)->load()){
            throw new FailedToLoadProjectException();
        }

        // Bootstrap the Project
        $project->boot();

        // Get the Agent
        dd($this->context->agents);


        // Run the solution
        $project->run();
    }
}
