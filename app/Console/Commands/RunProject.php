<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

use App\Context;
use App\Exceptions\FailedToLoadSolutionException;



class RunProject extends Command
{

    /**
     * Solution context File
     *
     * @var Context
     */
    protected $context;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'project:run {project} {solution}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Runs the specified solution in the Project';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Context $context)
    {
        $this->context = $context;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Init arguments
        $projectSlug = $this->argument('project');
        $solutionSlug = $this->argument('solution');

        // Start the project
        /** @var App\Solutions\SolutionInterface $solution */
        if (!$project = $this->context->project($projectSlug)->load()){
            throw new FailedToLoadProjectException();
        }

        // Bootstrap the Project
        $project->boot();

        // Run the solution
        $project->run($solutionSlug);
    }
}
