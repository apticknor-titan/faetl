<?php
namespace App;

use App\Agents\Agent;
use App\Models\Record;

use Webpatser\Uuid\Uuid;
use App\Maps\MapInterface;
use App\Maps\TransformationResult;
use App\Projects\ProjectInterface;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use App\Exceptions\CannotReadContextFileException;
use App\Exceptions\MalformedProjectConfigurationFile;

class Context
{
    /**
     * The disk name for where project configs are stored.
     *
     */
    const DISK_PROJECTS = 'projects';

    /**
     * Generic Meta Object
     *
     * @var Meta
     */
    public $meta;

    /**
     * The Project slug
     *
     * @var string|null
     */
    public $slug;

    /**
     * The solution slug
     *
     * @var string|null
     */
    public $solution;

    /**
     * The fully loaded project.
     *
     * @var ProjectInterface
     */
    public $project;

    /**
     * A stdObject of the decoded solution context file.
     *
     * @var mixed
     */
    private $decoded;

    /**
     * A UUID used for collecting all of the records created during this input session.
     *
     * @var string
     */
    public $sessionId;

    /**
     * Create a new Context instance
     *
     * @param string|null $projectSlug
     */
    public function __construct($projectSlug = null){
        $this->slug = $projectSlug;
        $this->sessionId = self::generateSessionId();
    }

    /**
     * Alias to UUID::generate
     *
     * @return string
     */
    public static function generateSessionId(){
        return utf8_encode(Uuid::generate());
    }

    /**
	 * Convert a project slug into a Project name
     * Ex:  Slug: sample
     *      Name: Sample
	 *
	 * @param string $slug
	 * @return string
	 */
    public static function projectFilename($slug){
		return $slug . ".json";
    }

    /**
	 * Return an instance of a project
	 *
     * This should be refactored to use a service provider
     *
	 * @param string $slug
	 * @return mixed
	 */
	public static function boot($slug)
	{
        $className = sprintf("App\Projects\%sProject", ucfirst($slug));
        return $className;
	}

    /**
     * Set the slug
     *
     * @param string $withSlug
     * @return self
     */
    public function project($withSlug = ""){
        $this->slug = $withSlug;

        return $this;
    }

    /**
     * Return the destination connector slug from the currently running solution
     *
     * @return string
     */
    public function getOutputId(){
        if (is_null($this->getRunningSolution())){
            $e = new \Exception();
            Log::debug($e->getTraceAsString());
            return null;
        }
        $solution = $this->project->solutions[$this->getRunningSolution()];
        return $solution->meta->destination ?? null;
    }

    /**
     * Resolve the slug into a project context file.
     *
     * @return string
     */
    private function contextFileName(){
        return self::projectFilename($this->slug);
    }

    /**
     * Return the contents of the context file.
     *
     * @throws CannotReadContextFileException
     * @return string
     */
    private function getContextFile(){
        $fileName = $this->contextFileName();
        if (Storage::disk(self::DISK_PROJECTS)->exists($fileName)){
            return Storage::disk(self::DISK_PROJECTS)->get($fileName);
        }

        throw new CannotReadContextFileException();
    }


    /**
     * Load and parse the project context
     *
     * @return void
     */
    public function load(){
        $this->project = $this->decode($this->getContextFile())
                        ->meta()
                        ->agents()
                        ->connectors()
                        ->solutions()
                        ->init();

        return $this->project;
    }


    /**
     * Decode json and store it.
     *
     * @param string $encoded
     * @return Context
     */
    protected function decode($encoded){
        $this->decoded = json_decode($encoded);

        return $this;
    }

    /**
     * Call specific section decoders
     *
     * @throws MalformedProjectConfigurationFile
     * @param string $section
     * @return Context
     */
    private function section($section){
        throw_if(!isset($this->decoded->$section), MalformedProjectConfigurationFile::class);

        $this->$section = $this->decoded->$section;

        return $this;
    }

    /**
     * Call the specific Meta section decoder
     *
     * @return Context
     */
    protected function meta(){
        return $this->section('meta');
    }

    /**
     * Call the specific Agents section decoder
     * Loop through each Agent and create a new Agent object
     * @return Context
     */
    protected function agents(){
        // 1 - Verify and preload the agents JSON
        $this->section('agents');

        // 2 - Loop through and init each one
        $agents = [];
        foreach ($this->agents as $agent){
            if (isset($agent->type)){
                $name = "App\\Agents\\" . ucfirst($agent->type);
                $agents[] = new $name($agent);
            } else {
                $agents[] = new Agent($agent);
            }
        }
        $this->agents = $agents;

        return $this;
    }

    /**
     * Call the specific Connector section decoder
     *
     * @return Context
     */
    protected function connectors(){
        return $this->section('connectors');
    }

    /**
     * Call the specific Solution section decoder
     *
     * @return Context
     */
    protected function solutions(){
        return $this->section('solutions');
    }

    /**
     * Return the bootstrapped Project object
     *
     * @return ProjectInterface
     */
    protected function init(){
        return self::boot($this->slug)::withContext($this);
    }

    /**
     * Sets the running solution
     *
     * @param string $solution
     * @return void
     */
    public function setRunningSolution($solution){
        Log::debug('Setting running solution:' . $solution);
        $this->solution = $solution;
    }

    /**
     * Gets the running solution
     *
     * @return string|null
     */
    public function getRunningSolution(){
        Log::debug('Getting running solution:' . $this->solution);
        return $this->solution;
    }

    /**
     * Return the Model class name for the type passed in.
     *
     * @param string $type
     * @return string
     */
    public static function modelForType($type){
        $className = "App\\Models\\" . ucfirst($type);
        return $className::make();
    }

    /**
     * Runs the next specified solution
     *
     * @param Record $next
     * @return mixed
     */
    public function next($next){
        if (gettype($next) == "string"){
            $solution = $next;
        } else {
            $solution = $next->solution;
        }

        $solution = $this->project->getNextSolution($solution);
        $project = $this->slug;

        return ['project' => $project, 'solution' => $solution];
    }
}
