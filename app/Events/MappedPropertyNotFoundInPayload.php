<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class MappedPropertyNotFoundInPayload
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * The record that contained the missing property
     *
     * @var mixed
     */
    public $record;

    /**
     * The missing property
     *
     * @var string
     */
    public $property;


    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($record, $property)
    {
        $this->record = $record;
        $this->property = $property;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('transformations');
    }
}
