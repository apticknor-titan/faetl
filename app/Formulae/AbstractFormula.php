<?php


namespace App\Formulae;


abstract class AbstractFormula implements FormulaInterface
{
    const COMPLEXITY = "simple";

    /**
     * The left side operand of the formula
     *
     * @var mixed
     */
    protected $operand;

    /**
     * Create a new instance of FormulaInterface with Input
     *
     * @param mixed $input
     */
    public function __construct($input = null){
        $this->setOperand($input);
    }

    /**
     * Convert the operand to the appropriate output.
     *
     * @return string
     */
    public function transform(){
        return $this->getOperand();
    }

    /**
     * Return the output from the translation operation
     *
     * @return mixed
     */
    public function getOutput(){
        return $this->transform();
    }

    /**
     * Set the operand
     *
     * @return void
     */
    public function setOperand($input = null){
        $this->operand = $input;
    }

    /**
     * Return the untranslated operand
     *
     * @return string
     */
    public function getOperand(){
        return $this->operand;
    }

    /**
     * Validate the Operand
     *
     * @throws MalformedOperandException
     * @return void
     */
    public function validateOperand(){
        return true;
    }
}
