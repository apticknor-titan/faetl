<?php
namespace App\Formulae;

class BigCommerceAppliedDiscountsFormula extends AbstractFormula
{
    /**
     * Convert the operand to the appropriate out.
     *
     * @return string
     */
    public function transform(){
        return (string)$this->getOperand();
    }
}
