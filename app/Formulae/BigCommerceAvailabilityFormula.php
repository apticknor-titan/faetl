<?php
namespace App\Formulae;

use Cknow\Money\Money;
use Illuminate\Support\Arr;

use App\Exceptions\MalformedOperandException;



class BigCommerceAvailabilityFormula extends ComplexFormula
{
    /**
     * Availability string for available products (Enabled)
     */
    const AVAILABILITY_AVAILABILE = "available";

    /**
     * Availability string for not available products (Disabled)
     */
    const AVAILABILITY_NOT_AVAILABLE = "disabled";

    /**
     * Availability string for preorder products
     */
    const AVAILABILITY_PREORDER = "preorder";

    /**
     * Create a new instance of SamplePriceForumla with Input
     *
     * @param mixed $input
     */
    public function __construct($input = null){
        $this->setOperand($input);
    }

    /**
     * Determine the availability string based on the state
     * This does not support "preorder" as a value
     *
     * @todo Implement preorder
     * @return string
     */
    public function transform(){
        $operand = $this->getOperand();
        // Get the first element of the array as the state should not require more than one property
        $value = reset($operand['input']);

        if (empty($value['input'])) {
            return self::AVAILABILITY_AVAILABILE;
        }

        // Return the corresponding availability string.
        return ($value ? self::AVAILABILITY_AVAILABILE : self::AVAILABILITY_NOT_AVAILABLE);
    }

    /**
     * Validate the Operand
     *
     * @throws MalformedOperandException
     * @return void
     */
    public function validateOperand(){
        // 1 - Check for valid data
        $operand = $this->getOperand();

        if (!gettype($operand) == "array"
        || Arr::has($operand, ['input', 'arguments', 'arguments.glue'])){
            throw new MalformedOperandException();
        }

        return true;
    }
}
