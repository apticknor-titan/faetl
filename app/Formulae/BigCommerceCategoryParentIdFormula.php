<?php
namespace App\Formulae;

use Cknow\Money\Money;
use Illuminate\Support\Arr;

use App\Exceptions\MalformedOperandException;



class BigCommerceCategoryParentIdFormula extends ComplexFormula
{

    /**
     * Create a new instance of SamplePriceForumla with Input
     *
     * @param mixed $input
     */
    public function __construct($input = null){
        $this->setOperand($input);
    }

    /**
     * Insert a default value for the parent ID
     *
     * EX:
     * array:4 [
     *   "property" => "parent_id"
     *   "keys" => array:1 [
     *       0 => "PARENT_ID"
     *   ]
     *   "input" => array:1 [
     *       0 => "2002092510241886"
     *   ]
     *   "arguments" => null
     *  ]
     *
     * @return string
     */
    public function transform(){
        $operand = $this->getOperand();
        if (isset($operand['input'][0])){
            $value = $operand['input'][0];
        } else {
            $value = 0;
        }

        return $value;
    }

    /**
     * Validate the Operand
     *
     * @throws MalformedOperandException
     * @return void
     */
    public function validateOperand(){
        // 1 - Check for valid data
        $operand = $this->getOperand();

        if (!gettype($operand) == "array"
        || Arr::has($operand, ['input', 'arguments', 'arguments.glue'])){
            throw new MalformedOperandException();
        }

        return true;
    }
}
