<?php


namespace App\Formulae;


class BooleanFormula extends AbstractFormula
{
    /**
     * Convert the operand to the appropriate out.
     *
     * @return string
     */
    public function transform(){
        $operand = $this->getOperand();
        if (gettype($operand) == "boolean"){
            return $operand;
        }

        return (boolean)$this->getOperand();
    }
}
