<?php
namespace App\Formulae;

use Liquid\Liquid;
use Liquid\Template;

abstract class ComplexFormula extends AbstractFormula
{
    const COMPLEXITY = "complex";

    /**
     * The Liquid template engine handler
     *
     * @var Template
     */
    public $template;

    /**
     * Create a new instance of FormulaInterface with Input
     *
     * @param mixed $input
     */
    public function __construct($input = null){
        $this->setOperand($input);
    }

    protected function template(){
        if (!$this->template){
            $this->template = new Template();
        }

        return $this->template;
    }

    /**
     * Alias to Liquid's register filter function
     *
     * @param string $name
     * @param Callable $callback
     * @return void
     */
    public function registerFilter($name, $callback){
        $this->template()->registerFilter($name, $callback);
        return $this;
    }

    /**
     * Replace {{variable}} with the appropriate parts using Liquid
     *
     * @param string $argument
     * @param string[] $parts
     * @return string
     */
    public function parseArgument($argument, $parts){
        $this->template()->parse($argument);
        $output = $this->template()->render($parts);
        return $output;
    }
}
