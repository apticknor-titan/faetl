<?php
namespace App\Formulae;

use Cknow\Money\Money;
use Illuminate\Support\Arr;

use App\Exceptions\MalformedOperandException;



class ConcatStringFormula extends ComplexFormula
{

    /**
     * Create a new instance of SamplePriceForumla with Input
     *
     * @param mixed $input
     */
    public function __construct($input = null){
        $this->setOperand($input);
    }

    /**
     * Combine all the arguments as a string.
     *
     * @return string
     */
    public function transform(){
        $operand = $this->getOperand();

        if (gettype($operand['input']) == "array"){
            $input = array_flatten($operand['input']);
        } else {
            $input = $operand['input'];
        }
        return join($operand['arguments']->glue, $input);
    }

    /**
     * Validate the Operand
     *
     * @throws MalformedOperandException
     * @return void
     */
    public function validateOperand(){
        // 1 - Check for valid data
        $operand = $this->getOperand();

        if (!gettype($operand) == "array"
        || Arr::has($operand, ['input', 'arguments', 'arguments.glue'])){
            throw new MalformedOperandException();
        }

        return true;
    }
}
