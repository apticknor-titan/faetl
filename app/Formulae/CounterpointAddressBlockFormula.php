<?php
namespace App\Formulae;

use Cknow\Money\Money;
use Illuminate\Support\Arr;

use App\Exceptions\MalformedOperandException;



class CounterpointAddressBlockFormula extends CounterpointDocumentBlockFormula
{
    /**
     * Create a new instance
     *
     * @param mixed $input
     */
    public function __construct($input = null){
        $this->setOperand($input);
        $defaultValues = [];

        $this->registerFilter('firstName', function ($name) {
            $nameParts = explode(' ', $name);
            return $nameParts[0] ?? '';
        });
        $this->registerFilter('lastName', function ($name) {
            $nameParts = explode(' ', $name);
            return $nameParts[1] ?? '';
        });

        parent::__construct($input, $defaultValues);
    }

}
