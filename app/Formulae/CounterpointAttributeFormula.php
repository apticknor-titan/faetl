<?php
namespace App\Formulae;

use Cknow\Money\Money;
use Illuminate\Support\Arr;

use App\Exceptions\MalformedOperandException;



class CounterpointAttributeFormula extends ComplexFormula
{

    /**
     * Create a new instance of SamplePriceForumla with Input
     *
     * @param mixed $input
     */
    public function __construct($input = null){
        $this->setOperand($input);
    }

    /**
     * Combine all the arguments as a string.
     *
     * @return string
     */
    public function transform(){
        $operand = $this->getOperand();
        return [
            'value' => join('',$operand['input']),
            'frontend_label' => $operand['arguments']->frontend_label,
            'code' => $operand['arguments']->code,
        ];
    }

    /**
     * Validate the Operand
     *
     * @throws MalformedOperandException
     * @return void
     */
    public function validateOperand(){
        // 1 - Check for valid data
        $operand = $this->getOperand();

        if (!gettype($operand) == "array"
        || Arr::has($operand, ['input', 'arguments', 'arguments.glue'])){
            throw new MalformedOperandException();
        }

        return true;
    }
}
