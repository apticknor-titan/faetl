<?php
namespace App\Formulae;

class CounterpointBooleanFormula extends SimpleFormula
{
    const SYNONYMS_TRUE = [
        "Y",    /* Y is for Yes */
        "A"     /* A is for Active */
    ];

    const SYNONYMS_FALSE = [
        "N",    /* N is for No */
        "I"     /* I is for Inactive */
    ];

    /**
     * Attempt to convert a Counterpoint boolean value to an actual boolean value
     *
     * @return boolean
     */
    public function transform(){
        $operand = $this->getOperand();

        // Explicitly check for true or false
        if (in_array($operand, self::SYNONYMS_TRUE)){
            return true;
        }

        if (in_array($operand, self::SYNONYMS_FALSE)){
            return false;
        }

        // Fallback to casting as a bool which will probably always be true.
        return (boolean)$this->getOperand();
    }
}
