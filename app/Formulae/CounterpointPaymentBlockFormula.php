<?php
namespace App\Formulae;

use Cknow\Money\Money;
use Illuminate\Support\Arr;

use App\Exceptions\MalformedOperandException;



class CounterpointPaymentBlockFormula extends CounterpointDocumentBlockFormula
{
    /**
     * Create a new instance
     *
     * @param mixed $input
     */
    public function __construct($input = null){
        $this->setOperand($input);

        $defaultValues = [
            "PAY_COD" => "CRCARD",
            "FINAL_PMT" => "N"
        ];

        parent::__construct($input, $defaultValues);
    }

}
