<?php
namespace App\Formulae;

use Cknow\Money\Money;
use Illuminate\Support\Arr;

use App\Exceptions\MalformedOperandException;



class CounterpointTierPriceFormula extends ComplexFormula
{
    /**
     * An array of properties to be merged in with the input arguments
     *
     * @var string[]
     */
    public $defaultValues;

    /**
     * Create a new instance of CounterpointOrderNotesFormula with Input
     *
     * @param mixed $input
     */
    public function __construct($input = null){
        $this->setOperand($input);
        $this->defaultValues = [];

        $this->registerFilter('toInteger', function ($value) {
            return intval($value);
        });
        $this->registerFilter('toFloat', function ($value) {
            return (float)$value;
        });

        parent::__construct($input);
    }

    /**
     * Converts the Order ID into a note
     *
     * @return string
     */
    public function transform(){
        $operand = $this->getOperand();

        $dataBlock = [];
        $parts = array_combine($operand['keys'], $operand['input']);
        foreach ($operand['arguments'] as $left => $right) {
            // @todo This is a hackjob. Liquid returns only strings
            $dataBlock[$left] = $this->parseArgument($right, $parts);
            if (stristr($right, 'toInteger') || stristr($left, 'quantity_max')){
                $dataBlock[$left] = intval($this->parseArgument($right, $parts));
            }
            if (stristr($right, 'toFloat')){
                $dataBlock[$left] = (float)$this->parseArgument($right, $parts);
            }

        }

        return $dataBlock;
    }

    /**
     * Validate the Operand
     *
     * @throws MalformedOperandException
     * @return void
     */
    public function validateOperand(){
        // 1 - Check for valid data
        $operand = $this->getOperand();

        // if (!gettype($operand) == "array"
        // || Arr::has($operand, ['input', 'arguments', 'arguments.code', 'arguments.frontend_label'])){
        //     throw new MalformedOperandException();
        // }

        return true;
    }
}
