<?php
namespace App\Formulae;

use Cknow\Money\Money;
use Illuminate\Support\Arr;

use App\Exceptions\MalformedOperandException;



class CounterpointTierPriceQuantityFormula extends ComplexFormula
{
    /**
     * An array of properties to be merged in with the input arguments
     *
     * @var string[]
     */
    public $defaultValues;

     /**
     * Create a new instance of SamplePriceForumla with Input
     *
     * @param mixed $input
     */
    public function __construct($input = null){
        $this->setOperand($input);
    }

    /**
     * Converts the Order ID into a note
     *
     * @return string
     */
    public function transform(){
        $operand = $this->getOperand();

        $dataBlock = [
            'quantity_min' => $operand['input'],
            'quantity_max' => 0
        ];

        return $dataBlock;
    }

    /**
     * Validate the Operand
     *
     * @throws MalformedOperandException
     * @return void
     */
    public function validateOperand(){
        // 1 - Check for valid data
        $operand = $this->getOperand();

        // if (!gettype($operand) == "array"
        // || Arr::has($operand, ['input', 'arguments', 'arguments.code', 'arguments.frontend_label'])){
        //     throw new MalformedOperandException();
        // }

        return true;
    }
}
