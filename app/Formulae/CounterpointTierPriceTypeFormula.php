<?php
namespace App\Formulae;

use Cknow\Money\Money;
use Illuminate\Support\Arr;

use App\Exceptions\MalformedOperandException;



class CounterpointTierPriceTypeFormula extends SimpleFormula
{
    /**
     * Counterpoint only supports one type of tier price: "fixed"
     *
     * @return string
     */
    public function transform(){
        return "fixed";
    }

}
