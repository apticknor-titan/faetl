<?php
namespace App\Formulae;

class CurrencyFormula extends AbstractFormula
{
    /**
     * Convert the operand to the appropriate out.
     *
     * @return string
     */
    public function transform(){
        return (float)$this->getOperand();
    }
}
