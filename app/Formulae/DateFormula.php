<?php
namespace App\Formulae;

class DateFormula extends AbstractFormula
{
    /**
     * Convert the operand to the appropriate out.
     *
     * @return string
     */
    public function transform(){
        return (string)$this->getOperand();
    }
}
