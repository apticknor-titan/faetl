<?php
namespace App\Formulae;

use Cknow\Money\Money;
use Illuminate\Support\Arr;

use App\Exceptions\MalformedOperandException;



class DirectRecordFormula extends AbstractFormula
{

    /**
     * Create a new instance of DirectRecordFormula with Input
     *
     * @param mixed $input
     */
    public function __construct($input = null){
        $this->setOperand($input);
    }

    /**
     * The direct record is already transformed.
     *
     * @return string
     */
    public function transform(){
        $operand = $this->getOperand();
        $operand = reset($operand);
        $operand = (array)$operand;

        return $operand;
    }

    /**
     * Validate the Operand
     *
     * @throws MalformedOperandException
     * @return void
     */
    public function validateOperand(){
        // 1 - Check for valid data
        $operand = $this->getOperand();

        if (!gettype($operand) == "array"
        || Arr::has($operand, ['data'])){
            throw new MalformedOperandException();
        }

        return true;
    }
}
