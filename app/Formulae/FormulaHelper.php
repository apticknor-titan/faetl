<?php


namespace App\Formulae;


class FormulaHelper
{

    const COMPLEX_FORMULA = "complex";
    const SIMPLE_FORMULA = "simple";

    /**
     * Perform the calculate
     *
     * @param string $type
     * @param string|string[] $operand
     * @return mixed
     */
    public static function resolve($formula, $operand = ""){
        $formulaClass = "App\\Formulae\\" . ucfirst($formula) . "Formula";

        $formula = new $formulaClass($operand);

        return $formula;
    }
}
