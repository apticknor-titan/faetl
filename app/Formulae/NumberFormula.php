<?php
namespace App\Formulae;

class NumberFormula extends ComplexFormula
{
    /**
     * Convert the operand to the appropriate out.
     *
     * @return string
     */
    public function transform(){
        $operand = $this->getOperand();
        $value = [
            $operand['property'] => $operand['input'][0] ?? $operand['arguments']->default ?? 0.00
        ];
        return $value;
    }
}
