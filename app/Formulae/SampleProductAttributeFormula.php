<?php
namespace App\Formulae;

use App\Exceptions\MalformedOperandException;
use Illuminate\Support\Arr;



class SampleProductAttributeFormula extends AbstractFormula
{

    /**
     * Create a new instance of SampleProductAttributeForumla with Input
     *
     * @param mixed $input
     */
    public function __construct($input = null){
        $this->setOperand($input);
    }

    /**
     * Convert the operand to the appropriate out.
     *
     * @return string
     */
    public function transform(){
        if ($this->validateOperand()){
            $value = $this->getOperand();
            return [
                'value' => $value['input'],
                'frontend_label' => $value['arguments']['frontend_label'],
                'code' => $value['arguments']['code'],
            ];
        }

        return [];
    }

    /**
     * Validate the Operand
     *
     * @throws MalformedOperandException
     * @return void
     */
    public function validateOperand(){
        // 1 - Check for valid data
        $operand = $this->getOperand();

        if (gettype($operand) == "array"
        && !Arr::has($operand, ['input', 'arguments.code', 'arguments.frontend_label'])){
            throw new MalformedOperandException();
        }

        return true;
    }
}
