<?php
namespace App\Formulae;

use Cknow\Money\Money;
use Illuminate\Support\Arr;

use App\Exceptions\MalformedOperandException;



class SampleTierPriceFormula extends AbstractFormula
{

    /**
     * Create a new instance of SamplePriceForumla with Input
     *
     * @param mixed $input
     */
    public function __construct($input = null){
        $this->setOperand($input);
    }

    /**
     * Convert a price from a string value to decimal(12,2)for Magento 2.
     *
     * @return string
     */
    public function transform(){
        $amount = $this->getOperand();
        return [
            'amount' => Money::parse($amount[0])->formatByDecimal(),
            'tier_qty' => $amount[1]
        ];
    }

    /**
     * Validate the Operand
     *
     * @throws MalformedOperandException
     * @return void
     */
    public function validateOperand(){
        // 1 - Check for valid data
        $operand = $this->getOperand();

        if (!gettype($operand) == "array"
        || Arr::has($operand, ['input', 'arguments', 'arguments.code', 'arguments.frontend_label'])){
            throw new MalformedOperandException();
        }

        return true;
    }
}
