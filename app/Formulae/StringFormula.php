<?php
namespace App\Formulae;

class StringFormula extends AbstractFormula
{
    /**
     * Convert the operand to the appropriate out.
     *
     * @return string
     */
    public function transform(){
        $value = $this->getOperand();
        if ($value === null){
            return $value;
        }
        return (string)$value;
    }
}
