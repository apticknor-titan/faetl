<?php

namespace App\Http\Controllers;

use App\Context;
use App\Models\JobStatus;
use Illuminate\Http\Request;

use App\Projects\ProjectInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Exceptions\FailedToLoadSolutionException;


class JobController extends Controller
{
    /**
     * Solution context File
     *
     * @var Context
     */
    protected $context;

    /**
     * The Booted Project
     *
     * @var ProjectInterface $project
     */
    protected $project;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Context $context)
    {
        $this->context = $context;
    }

    /**
     * Return Job Status for given job id
     *
     * @param Request $request
     * @param string $project
     * @param string $environment
     * @param string $solution
     * @param string $job_id
     * @return string
     */
    public function show(Request $request, $project, $environment, $job_id){
        $job = JobStatus::where('key', $job_id)->firstOrFail();
        return $job->toApiResponse();
    }
}

