<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use App\Context;
use App\Projects\ProjectInterface;
use App\Exceptions\FailedToLoadSolutionException;


class ProjectController extends Controller
{
    /**
     * Solution context File
     *
     * @var Context
     */
    protected $context;

    /**
     * The Booted Project
     *
     * @var ProjectInterface $project
     */
    protected $project;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Context $context)
    {
        $this->context = $context;
    }

    /**
     * Boot the project
     *
     * TODO: This should be refactored into middleware
     * @param string $projectSlug
     * @param string $solutionSlug
     * @return void
     */
    public function boot($projectSlug){
        if ($this->project){
            return $this->project;
        }

        // Start the project
        /** @var App\Solutions\SolutionInterface $solution */
        if (!$project = $this->context->project($projectSlug)->load()){
            throw new FailedToLoadProjectException();
        }

        // Bootstrap the Project
        $project->boot();

        $this->project = $project;
    }

    /**
     * Describe the project to the API User
     *
     * @param Request $request
     * @param string $project_slug
     * @return string
     */
    public function describe(Request $request, $project_slug){
        // Bootstrap the Project
        $this->boot($project_slug);

        // Output the project description
        return response()->json($this->project->describe());
    }

    /**
     * Run a solution with request data
     *
     * @param Request $request
     * @param string $project
     * @param string $environment
     * @param string $solution
     * @return string
     */
    public function run(Request $request, $project, $environment, $solution){
        // Bootstrap the Project
        $this->boot($project);

        // Run the requested solution and return the result.
        $result = response()->json($this->project->run($solution));
        return $result;
    }

    /**
     *
     */
    public function sample(){
        $response = json_decode(Storage::disk('projects')->get('eccategories.json'));
        return response()->json($response);
    }
}

