<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use App\Context;
use App\Projects\ProjectInterface;
use App\Exceptions\FailedToLoadSolutionException;


class WebhookController extends Controller
{
    /**
     * Solution context File
     *
     * @var Context
     */
    protected $context;

    /**
     * The Booted Project
     *
     * @var ProjectInterface $project
     */
    protected $project;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Context $context)
    {
        $this->context = $context;
    }

    /**
     * Boot the project
     *
     * TODO: This should be refactored into middleware
     * @param string $projectSlug
     * @param string $solutionSlug
     * @return void
     */
    public function boot($projectSlug){
        if ($this->project){
            return $this->project;
        }

        // Start the project
        /** @var App\Solutions\SolutionInterface $solution */
        if (!$project = $this->context->project($projectSlug)->load()){
            throw new FailedToLoadProjectException();
        }

        // Bootstrap the Project
        $project->boot();

        $this->project = $project;
    }

    /**
     * Run a solution with request data
     *
     * @param Request $request
     * @param string $project_slug
     * @param string $solution_slug
     * @return void
     */
    public function run(Request $request, $project_slug, $solution_slug){
        // Bootstrap the Project
        $this->boot($project_slug);

        // Run the requested solution and return the result.
        return response()->json($this->project->run($solution_slug));
    }
}

