<?php
namespace App\Maps;

use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use App\Formulae\FormulaHelper;
use App\Maps\AbstractMap;
use App\Models\Product;
use App\Records\RecordInterface;
use App\Exceptions\CouldNotResolveDynamicType;

class Map extends AbstractMap
{
    /**
     * Dynamic type placeholder
     */
    const TYPE_DYNAMIC = "dynamic";

    /**
     * Formula Helper
     *
     * @var FormulaHelper
     */
    private $formulaHelper;

    /**
     * The Map Schema
     * Contains all of the fields, their destination field path and
     * the formula/attributes
     *
     * @var string
     */
    public $schemas;

    /**
     * The name of the schema being used.
     *
     * @var string
     */
    public $name;

    /**
     * Create a new MapInterface instance
    */
    public function __construct($schemas, $name = 'default'){
        $this->formulaHelper = new FormulaHelper();
        $this->schemas = self::schemas($schemas);
        $this->name = $name;
    }

    /**
     * Shim in the original schema call
     *
     * @param string $key
     * @return string[]|mixed
     */
    public function __get($key){
        if ($key == "schema"){
            return $this->schemas[$this->name];
        }

        $trace = debug_backtrace();
        trigger_error(
            'Undefined property via __get(): ' . $name .
            ' in ' . $trace[0]['file'] .
            ' on line ' . $trace[0]['line'],
            E_USER_NOTICE);
        return null;
    }

    /**
     * Dynamic fields get their type from the payload
     * If the payload has a type property, use that.
     *
     * @param string[] $field
     * @param string $value
     * @return void
     * @throws CouldNotResolveDynamicType
     */
    public static function resolveDynamicField(&$field, $value){
        if ($field->type == self::TYPE_DYNAMIC){
            if (isset($value['type'])){
                $field->type = $value['type'];
            } else {
                throw new CouldNotResolveDynamicType();
            }
        }
    }

    /**
     * Create an associative array of all of the schemas and their names
     *
     * Ex.
     * {
     *  "default": {
     *  },
     *  "products": {
     *  }
     * }
     *
     *
     * @param string[] $schemas
     * @return string[]
     */
    public static function schemas($schemas){
        $array = [];
        foreach ($schemas as $id => $schema){
            $key = array_key_first((array)$schema);
            $array[$key] = $schema->$key;
        }

        return $array;
    }

    /**
     * Process the given candidate
     * The transformation candidate is formatted:
     * [
     *  'key1' => 'value1',
     *  'key2' => 'value2',
     *  ...
     *  'keyN' => 'valueN'
     * ]
     *
     * The array keys should be concatinated with a comma:
     * 'key1,key2,keyN'
     *
     * Then find the formula for that key and pass in an array of arguments
     * containing the values from the array
     *
     * @param mixed $field
     * @param string[] $candidate
     * @return void
     */
    public function process($field, $candidate){
        // @todo PHP does not differentiate between non-existence and null. Refactor to allow null values some how.z
        if (empty($candidate)){
            return false;
        }

        // Preprocess some of the requirements
        $schema = $this->schemas[$this->name];

        // Generate the default value for the formula
        $value = Arr::first($candidate);

        // Load the Forumla
        $formula = $this->formulaHelper->resolve($field->formula);

        // Build complex formula arguments
        if ($formula::COMPLEXITY == FormulaHelper::COMPLEX_FORMULA){
            $field->arguments = Arr::first((array)($field->arguments ?? []));
            $value = [
                'property' => $field->property,
                'keys' => array_keys($candidate),
                'input' => array_values($candidate),
                'arguments' => $field->arguments
            ];
        }
        $formula->setOperand($value);

        // Perform the transformation
        $transformed = $formula->transform();

        // Resolve dynamic types
        self::resolveDynamicField($field, $transformed);

        // Construct the result
        $transformationResult = new TransformationResult(
            $field->type,
            $field->property,
            $field->unique ?? false,
            $transformed
        );

        return $transformationResult;
    }
}
