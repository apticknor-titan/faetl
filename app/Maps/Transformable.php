<?php
namespace App\Maps;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;



class Transformable
{
    /**
     * The prefix for getting a property after transformation.
     *
     */
    const TRANSFORMED_PREFIX = "transformed";

    /**
     * The transformation schema. An object with key->value pairs
     * that represent the property and how it is to be transformed
     *
     * @var string[]
     */
    public $schema;

    /**
     * The map object
     * @var mixed
     */
    public $map;

    /**
     * The model being transofmred
     * @var Eloquent
     */
    public $model;

    /**
     * The array of data values that have already been transformed.
     * @var string[]
     */
    private $transformed;

    /**
     * Create a new instance of Transformable.
     *
     * @param Eloquent $model
     * @param string $schema
     */
    public function __construct(Eloquent $model, $schema){
        $this->model = $model;
        $this->schema = $schema;

    }

    /**
     * Intercept getting a value from the model and transform it.
     *
     *
     * @param string $property
     * @return mixed
     */
    public function __get($property){
        if (!isset($this->model->$property)){
            return null;
        }

        return $this->model->$property;
    }
}
