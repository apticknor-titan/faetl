<?php
namespace App\Maps;

class TransformationResult
{
    /**
     * The destination USDF Model
     *
     * @var string
     */
    public $type;

    /**
     * The destination USDF Field
     *
     * @var [type]
     */
    public $field;

    /**
     * The transformed value
     *
     * @var mixed
     */
    public $transformed;

    /**
     * The result should produce a unique model
     *
     * @var bool
     */
    public $unique;

    /**
     * The transformed arguments
     *
     * @var mixed
     */
    public $arguments;

    public function __construct($type, $field, $unique, $transformed, $arguments = []){
        $this->type = $type;
        $this->field = $field;
        $this->unique = $unique;
        $this->transformed = $transformed;
        $this->arguments = $arguments;
    }
}
