<?php
namespace App\Maps;

use Illuminate\Support\Arr;

class TransformationStack extends \ArrayObject
{

    /**
     * The state of the transformation
     *
     * For backward compatibility, it should be false.
     *
     * @var boolean
     */
    protected $failed = false;

    /**
     * Combine two Stacks
     *
     * @param TransformationStack $firstStack
     * @param TransformationStack $secondStack
     * @return void
     */
    public static function merge($firstStack, $secondStack){
        return new self(array_merge((array)$firstStack, (array)$secondStack));
    }

    /**
     * Return the property as an element of an array
     *
     * @param string $property
     * @return mixed
     */
    public function __get($property){
        return $this[$property];
    }

    /**
     * Return or set the failed state of the transformation
     *
     * @return bool
     */
    public function failed($failed = null){
        if (is_null($failed)){
            return $this->failed;
        }

        $this->failed = $failed;

        return $failed;
    }

    /**
     * Save every method in the stack.
     *
     * @return void
     */
    public function save(){
        foreach ($this as $model){
            $model->save();
        }
    }

    /**
     * Return all of the result data as an array, recursive
     *
     * @return string[]
     */
    public function toArray(){
        $stack = [];
        foreach ($this as $model){
            $stack = array_merge($model->toArray(), $stack);
        }

        return $stack;
    }

    /**
     * Run a function on each element
     *
     * @param Callable $callable
     * @return void
     */
    public function map(Callable $callable){
        foreach ($this as $element){
            $callable($element);
        }
    }

    /**
     * Return an array of all models by the given type
     *
     * @param string $type
     * @return mixed
     */
    public function getModelsByType($type){
        $stack = [];
        foreach ($this as $model){
            if (get_class($model) == $type){
                $stack[] = $model;
            }
        }

        return $stack;
    }
}
