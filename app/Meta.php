<?php
namespace App;

use Illuminate\Support\Facades\Storage;

use App\Context;

class Meta
{
    /**
     * Create a meta object with props
     *
     * @param Context $withContext
     */
    public function __construct($withContext){
        foreach ($withContext->meta as $prop => $value){
            $this->$prop = $value;
        }
    }

    /**
     * Determine if the context is output to ERP or input to platform
     *
     * Default is false.
     *
     * @return boolean
     */
    public function isOutput(){
        return $this->output ?? false;
    }
}
