<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Address extends Eloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'input_id',
        'output_id',
        'status',
        'reference_id',
        'reference_type',
        'first_name',
        'last_name',
        'name',
        'street1',
        'street2',
        'city',
        'region',
        'postcode',
        'country',
        'phone',
        'email',
    ];
}
