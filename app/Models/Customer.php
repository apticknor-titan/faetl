<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Customer extends Eloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'input_id',
        'output_id',
        'customer_group',
        'first_name',
        'middle_name',
        'last_name',
        'phone',
        'email',
        'gender',
        'date_of_birth',
        'state',
        'passsword',
        'weight',
        'length',
        'width',
        'height',
        'manufacturer',
    ];
}
