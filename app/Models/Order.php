<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

use App\Traits\Transformable;

class Order extends Eloquent
{
    use Transformable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'input_id',
        'output_id',
        'customer_id',
        'order_created_at',
        'status',
        'customer_first_name',
        'customer_last_name',
        'bill_name',
        'bill_street1',
        'bill_street2',
        'bill_city',
        'bill_region',
        'bill_postcode',
        'bill_country',
        'bill_phone',
        'ship_name',
        'ship_street1',
        'ship_street2',
        'ship_city',
        'ship_region',
        'ship_postcode',
        'ship_country',
        'ship_phone',
        'payment_method',
        'shipping_method',
        'payment_amount',
        'shipping_amount',
        'tax_amount',
        'subtotal_amount',
        'grand_total_amount',
    ];
}
