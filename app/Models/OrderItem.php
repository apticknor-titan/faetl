<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class OrderItem extends Eloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'input_id',
        'output_id',
        'product_id',
        'order_id',
        'name',
        'description',
        'meta_description',
        'price_amount',
        'discount_amount',
        'tax_amount',
        'shipping_amount',
        'weight',
        'length',
        'width',
        'height',
    ];
}
