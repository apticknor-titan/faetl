<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Price extends Eloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'input_id',
        'output_id',
        'customer_id',
        'product_id',
        'product_group',
        'price',
        'percentage',
        'tier_qty',
        'type',
        'start_at',
        'end_at',
    ];
}
