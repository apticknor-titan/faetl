<?php
namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Product extends Eloquent
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'input_id',
        'output_id',
        'price_id',
        'sku',
        'name',
        'subtitle',
        'description',
        'meta_description',
        'state',
        'quantity',
        'tax_class',
        'weight',
        'length',
        'width',
        'height',
        'manufacturer',
    ];



}
