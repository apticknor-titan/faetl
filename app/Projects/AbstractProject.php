<?php
namespace App\Projects;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;

use App\Meta;
use App\Models\Record;
use App\Actions\ActionPayload;
use App\Actions\RunSolutionAction;
use App\Exceptions\SolutionDoesNotExist;
use App\Exceptions\MethodCouldNotProceed;


class AbstractProject implements ProjectInterface
{
    /**
     * The solution context
     *
     * @var mixed
     */
    protected $context;

    /**
     * The Meta instance
     * @var App\Meta
     */
    public $meta;

    /**
     * Array of Connectors
     * @var App\Connectors\ConnectorInterface[]
     */
    public $connectors;

    /**
     * The Solutions
     * @var string[]
     */
    public $solutions;

    /**
     * Create a new instance of ProjectInterface
     */
    public function __construct(){

    }

    /**
     * Create a new instance of ProjectInterface with Context
     *
     * @param Context $context
     * @return ProjectInterface
     */
    public static function withContext($context){
        $instance = new self();
        $instance->setContext($context);
        return $instance;
    }

    /**
     * Create a new instance of ConnectorInterface with context.
     *
     * @return ConnectorInterface[]
     */
    public static function connectors($withContext){
        $connectors = [];
        $connectorsArray = $withContext->connectors;

        foreach ($connectorsArray as $data){
            $connectorClassName = connector($data->type);
            $connectors[$data->meta->slug] = new $connectorClassName($data);
        }

        return $connectors;
    }

    /**
     * Create a new instance of all of the solutions.
     *
     * @return SolutionInterface[]
     */
    public static function solutions($withContext){
        $solutions = [];
        $solutionsArray = $withContext->solutions;

        foreach ($solutionsArray as $data){
            $solutions[$data->meta->slug] = $data;
        }

        return $solutions;
    }

    /**
     * Create a new Meta instance.
     *
     * @return Meta
     */
    public static function meta($withContext){
        $metaInstance = new Meta($withContext);
        return $metaInstance;
    }

    /**
     * Context setter
     *
     * @param Context $context
     * @return void
     */
    public function setContext($context){
        $this->context = $context;
    }

    /**
     * Context getter
     *
     * @return Context
     */
    public function getContext(){
        return $this->context;
    }

    /**
     * Setup the Project
     *
     * @return void
     */
    public function boot(){
        $this->meta = self::meta($this->context);
        $this->connectors = self::connectors($this->context);
        $this->solutions = self::solutions($this->context);
    }

    /**
     * Run the solution
     *
     * @param string $slug
     * @return void
     */
    public function run($slug, ...$arguments){
        // 0 - Preload/Init
        Log::debug('Running solution: ' . $slug);
        $this->slug = $slug;
        $context = context();

        // 1. Determine if Solution exists
        if (!$this->solutionExists()){
            throw new SolutionDoesNotExist();
        }

        $context->setRunningSolution($slug);

        $result = null;

        // 2. Loop through the workflow and execute each function
        foreach ($this->getWorkflow() as $workflow => $method){
            Log::debug('Workflow method: ' . $method->method);
            $result = $this->proceed($method, $result, ...$arguments);
        }

        // 3 - Run next solution
        $next = $context->next($slug);
        if ($next['project'] === false || $next['solution'] === false){
            Log::debug('Next solution not defined. Ending workflow here.');
            return $result;
        }

        Log::debug('Running next solution: ' . join('.', $next));

        $payloadRecord = Record::make([
            'input_id' => $context->sessionId,
            'data' => '',
            'project' => $next['project'],
            'solution' => $next['solution']
        ]);

        $payload = new ActionPayload($payloadRecord);

        dispatch(new RunSolutionAction($payload));

        return $result;
    }

    /**
     * Execute the method with parameters
     *
     * @param mixed $method
     * @param mixed $previous
     * @param mixed $arguments
     * @throws MethodCouldNotProceed
     * @return mixed
     */
    public function proceed($method, $previous, ...$arguments){
        // 1 - Get Connection specified in method
        $connection = $this->connectors[$method->source];

        // 2 - Combine properties from the context file as well as records being passed in.
        $methodProps = (array)($method->properties ?? []);
        $methodProps = array_merge($methodProps, ['previous' =>  $previous]);
        $methodProps = array_merge($methodProps, $arguments);

        // 3 - Call the method on the connection
        return $connection->{$method->method}(...array_values($methodProps));
    }

    /**
     * Verify the solution exists
     *
     * @return bool
     */
    public function solutionExists(){
        if ($this->slug && isset($this->solutions[$this->slug])){
            return true;
        }

        return false;
    }

    /**
     * Return the workflow for the solution
     *
     * @param mixed $solution
     * @return string[]
     */
    public function getWorkflow($solution = null){
        return $this->solutions[$solution ?? $this->slug]->workflow;
    }

    /**
     * Returns the map for the solution path
     *
     * @param string $solution
     * @return string[]
     */
    public function getSchema($solution = null){
        $solution = $solution ?? $this->context->getRunningSolution();
        return $this->solutions[$solution]->map;
    }

    /**
     * Find the Next prop within the workflow.
     *
     * @param mixed $solution
     * @return string|bool
     */
    public function getNextSolution($solution = null){
        $solution = $solution ?? $this->context->getRunningSolution();
        foreach ($this->getWorkflow($solution) as $method){
            if (isset($method->properties->next)){
                return $method->properties->next;
            }
        }

        return false;
    }

    /**
     * Return an array with a properly formatted description
     * of the project.
     *
     * @return string[]
     */
    public function describe(){
        $description = [];

        // 1 - Meta
        $description['meta'] = $this->meta;

        // 2 - Solutions
        $description['solutions'] = $this->solutions;

        return $description;
    }
}
