<?php

namespace App\Providers;

use App\Context;

use Illuminate\Support\ServiceProvider;

class ProjectServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any Project services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any Project services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->singleton(Context::class, function ($app) {
            return new Context();
        });

        $this->app->alias(Context::class, 'context');
    }


}
