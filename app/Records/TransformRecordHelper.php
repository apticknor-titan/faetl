<?php
namespace App\Records;

use App\Context;
use App\Maps\Map;
use App\Models\Price;
use App\Models\Record;
use App\Models\Product;
use App\Models\Attribute;
use App\Actions\ActionPayload;
use App\Actions\AbstractAction;
use App\Maps\TransformationStack;
use Illuminate\Support\Facades\Log;

use App\Events\MappedPropertyNotFoundInPayload;
use App\Exceptions\FailedToLoadProjectException;

class TransformRecordHelper
{
    /**
     * The separator between the parent object and the child object property name
     * Ex. "products.name"
     */
    const NESTED_PROPERTY_SEPARATOR = '.';

    /**
     * The separator between two or more mapped properties. Used with a ComplexFormula.
     * Ex. "customer_first_name,customer_last_name" gets sent to ConcatStringFormula as
     * [
     *  '0' => 'customer_first_name',
     *  '1' => 'customer_last_name'
     * ]
     */
    const MULTI_KEY_SEPARATOR = ',';

    /**
     * The resource
     *
     * @var mixed $resource
     */
    public $resource;

    /**
     * Create a new instance
     *
     * @param mixed $resource
     */
    public function __construct($resource){
        $this->resource = $resource;
    }

    /**
     * Transform the record.
     *
     * @return void
     */
    public function handle(){
        // 0 - Bootstrap the Project
        $context = resolve('context');
        $record = $this->resource;
        $context->setRunningSolution($record->solution);

        /** @var App\Solutions\SolutionInterface $solution */
        if (!$project = $context->project($record->project)->load()){
            throw new FailedToLoadProjectException();
        }

        $project->boot();
        /*
            inputId is the parent ID for all of the records created by ingesting this record.
            For example, prices are associated with a product. By using the same inputId
            for all price records, they can be reconstituted to the same product model
            on the otherside of the integration
        */
        $inputId = $record->input_id;

        /*

        */
        $output_id = $context->getOutputId();

        // Stack holds all of the records created by ingesting this record.
        $stack = new TransformationStack();

        // 0 - Instance Salt is used for creating multiple records of the same type.
        $instanceSalt = Context::generateSessionId();

        // 1 - Get Map for Solution
        $schema = $project->getSchema($record->solution);
        $map = new Map($schema, $record->map ?? 'default');

        // 2 - Convert each field using the formula in the schema
        $data = json_decode($record->data);

        // 3 - Required field counter.
        /*
            At the start of the loop, add the field to an array if it's required.
            At the end of the loop, remove the field from the array if a value
            was found in the payload. If a field remains in the array after the
            end of the loop, reject the record and report it.
        */
        $required = [];

        foreach ($map->schema as $key => $props){ /* foreach - Schema Loop */
            // Clone for backwards compatibility
            $field = $props;

            // Check for Nested Map
            if (isset($props->map)){
                // Split multi-key props
                if (self::isMultikeyProperty($key)){
                    $nestedProps = explode(self::MULTI_KEY_SEPARATOR, $key);
                    $nestedMultikeyData = [];
                    foreach ($nestedProps as $nestedKey){ /* foreach - Multi-key properties */

                        // Add field to required heap if field is required
                        if (isset($field->required) && $field->required === true){
                            $required[$nestedKey] = true;
                        }

                        // Nested payload might not have the key.
                        if (!isset($data->$nestedKey)){
                            continue; /* continue - Multi-key properties */
                        }

                        $nestedMultikeyData[$nestedKey] = $data->$nestedKey;

                        // Key has a value in this payload, remove it from the required heap.
                        unset($required[$nestedKey]);
                    }   /* endforeach - Multi-key properties */

                    // Check nested required fields
                    if (count($required) > 0){
                        // TODO report missing keys
                        Log::debug('[Nested, Multikey] Required keys are missing: ' . join(', ', array_keys($required)));
                        unset($nestedMultikeyData);
                        continue; /* continue - Schema Loop */
                    }

                    // If the nested payload is empty, don't create an empty model
                    // NestedData is an array of models. Each one becomes a model in a transformation stack
                    if (!empty($nestedMultikeyData)){
                        $nestedData = [$nestedMultikeyData];
                    } else {
                        $nestedData = $nestedMultikeyData;
                    }
                } else {
                    // Add field to required heap if field is required
                    if (isset($field->required) && $field->required === true){
                        $required[$key] = true;
                    }

                    // The payload might not have the key.
                    if (!isset($data->$key)){
                        continue; /* continue - Schema Loop */
                    }

                    $nestedData = $data->$key;
                    unset($required[$key]);
                }

                // Workaround for single product array that got squashed in the import.
                if (!is_array($nestedData)){
                    $nestedData = [$nestedData];
                }

                // Check nested required fields
                if (count($required) > 0){
                    // TODO report missing keys
                    Log::debug('[Nested] Required keys are missing: ' . join(', ', array_keys($required)));
                    unset($nestedData);
                    continue; /* continue - Schema Loop */
                }

                // Loop through each record in the nested data (Ex. Multiple products)
                foreach ($nestedData as $nestedRecord){
                    // 1a - Create a new record with the retrieved data model
                    $payloadRecord = Record::make([
                        'input_id' => $inputId,
                        'data' => json_encode($nestedRecord),
                        'project' => $record->project,
                        'solution' => $record->solution,
                        'map' => $props->map
                    ]);

                    $payloadRecord->save();

                    // Result is a transformation stack. In order to pass on the save expectation to the connect
                    // the stack needs to be merged. There is no elegant way to merge two ArrayObjects.
                    $helper = new TransformRecordHelper($payloadRecord);
                    $resultStack = $helper->handle();
                    $stack = new TransformationStack(array_merge((array)$stack, (array)$resultStack));
                } /* endforeach - Nested Map */
                continue;
            }

            // 3 - Split Multi-key maps
            $recordProperties = explode(self::MULTI_KEY_SEPARATOR, $key);
            $transformCandidate = [];

            foreach ($recordProperties as $property){
                // Add field to required heap if field is required
                if (isset($field->required) && $field->required === true){
                    $required[$property] = true;
                }

                // 3a - Verify the property exists and return it if it does.
                if ($value = self::getPropertyValue($data, $property)){
                    // 3b - Collect the value and property into an array
                    $transformCandidate[$property] = $value;
                    unset($required[$property]);
                    continue;
                }

                // 3c - Property in Map does not exist.
                // TODO - Enable this when ready
                // event(new MappedPropertyNotFoundInPayload($record, $property));
            }

            // Check nested required fields
            if (count($required) > 0){
                // TODO report missing keys
                Log::debug('Required keys are missing: ' . join(', ', array_keys($required)));
                unset($transformCandidate);
                $stack = new TransformationStack();
                $stack->failed(true);
                return $stack;
            }

            // 4 - Process the candidate
            $result = $map->process($field, $transformCandidate);

            // 5 - Create a stack of all of the transformed fields
            /** @var App\Maps\TransformationResult $result */
            if ($result){
                // 6 - Make a new instance of each Model.
                if ($result->unique){
                    $salt = Context::generateSessionId();
                } else {
                    $salt = $instanceSalt;
                }
                $type = $result->type . "-" . $salt;
                $stack[$type] = $stack[$type] ?? Context::modelForType($result->type);
                $stack[$type]->input_id = $stack[$type]->input_id ?? $inputId;
                $stack[$type]->output_id = $output_id;

                // A Complex Formula may return an array of transformed values
                if (is_array($result->transformed)){
                    foreach ($result->transformed as $result_key => $result_value){
                        $stack[$type]->{$result_key} = $result_value;
                    }
                } else {
                    $stack[$type]->{$result->field} = $result->transformed;
                }
            }
        } /* endforeach - Schema loop */

        // 7 - Discard the record for this instance
        $record->delete();

        // 9 - Return the stack.
        return $stack;
    }

    /**
     * Checks if a property is nested
     * A nested property has a period in the property string.
     * Nested properties are arrays of objects nested within the parent record.
     *
     * Ex. In BigCommerce, an order document returns all of the order items as
     * an array of product objects.
     * Thus, products.name as a map property refers to $order->products[index]->name
     *
     * @param mixed $property
     * @return boolean
     */
    public static function isNestedProperty($property){
        if (stristr($property, self::NESTED_PROPERTY_SEPARATOR)){
            return true;
        }

        return false;
    }

    /**
     * Checks if a property is multikey.
     *
     * Ex. ALT_1_UNIT,ALT_1_NUMER,ALT_1_DENOM,ALT_1_PRC_1
     *
     * @param mixed $property
     * @return boolean
     */
    public static function isMultikeyProperty($property){
        if (stristr($property, self::MULTI_KEY_SEPARATOR)){
            return true;
        }

        return false;
    }

    /**
     * Determine if the property exists in the data.
     * Supports recursive nested properties
     *
     * @param mixed $data
     * @param string $property
     * @return boolean
     */
    public static function getPropertyValue($data, $property){
        // 1 - Check for exact match
        if (isset($data->$property)){
            return $data->$property;
        }

        // 2 - Create a list of properties to check
        $stack = [];
        $propertyParts = explode(self::NESTED_PROPERTY_SEPARATOR, $property);

        $result = $data;
        $map = function ($result, $item){
            if (isset($result->$item)){
                return $result->$item;
            } else {
                return false;
            }
        };

        // 3 - Map each part to check for the nested properties
        foreach ($propertyParts as $nestedProperty){
            $result = $map($result, $nestedProperty);
            if (!$result){
                return false;
            }
        }

        return $result;
    }

    /**
     * Remove key from required if it exists
     *
     * @param string $key
     * @param string[] $required
     * @return string[]
     */
    public function updateRequiredStack($key){


    }

    /**
     * Undocumented function
     *
     * @return boolean
     */
    public function isRequiredStackEmpty(){

    }
}
