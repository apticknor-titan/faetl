<?php
namespace App\Solutions;

use App\Meta;

class SampleSolution extends AbstractSolution implements SolutionInterface
{

    public $supports = [
        'product',
    ];

    /**
     * The solution context
     *
     * @var mixed
     */
    protected $context;

    /**
     * The Map instance
     * @var App\Maps\MapInterface
     */
    public $map;

    /**
     * The Meta instance
     * @var App\Meta
     */
    public $meta;

    /**
     * The Connector instance
     * @var App\Connectors\ConnectorInterface
     */
    public $connector;

    /**
     * The Routine String
     * @var string
     */
    public $routine;

    /**
     * Create a new instance of SolutionInterface
     */
    public function __construct(){

    }

    /**
     * Create a new instance of SolutionInterface with Context
     *
     * @param Context $context
     * @return SolutionInterface
     */
    public static function withContext($context){
        $instance = new self();
        $instance->setContext($context);
        return $instance;
    }

    /**
     * Create a new instance of MapInterface with map data
     *
     * @return MapInterface
     */
    public static function map($withContext){
        $mapClassName = $withContext->meta->map;
        $mapIntance = new $mapClassName($withContext->map);
        return $mapIntance;
    }

    /**
     * Create a new instance of ConnectorInterface with context.
     *
     * @return ConnectorInterface
     */
    public static function connector($withContext){
        $connectorClassName = $withContext->meta->connector;
        $connectorInstance = new $connectorClassName($withContext);
        return $connectorInstance;
    }

    /**
     * Create a new Meta instance.
     *
     * @return Meta
     */
    public static function meta($withContext){
        $metaInstance = new Meta($withContext);
        return $metaInstance;
    }

    /**
     * Context setter
     *
     * @param Context $context
     * @return void
     */
    public function setContext($context){
        $this->context = $context;
    }

    /**
     * Context getter
     *
     * @return Context
     */
    public function getContext(){
        return $this->context;
    }

    /**
     * Return a resolved action method name
     *
     * @param string $actionSlug
     * @return string
     */
    public function action($actionSlug){
        return sprintf($actionSlug);
    }

    /**
     * Setup the Solution
     *
     * @return void
     */
    public function init(){
        $this->meta = self::meta($this->context);
        $this->map = self::map($this->context);
        $this->connector = self::connector($this->context);
    }

    /**
     * Run the solution
     *
     * @param string $routine
     * @return void
     */
    public function run($routine){
        $this->routine = $routine;

        return $this->$routine();
    }
}
