<?php
namespace App\Traits;

trait Transformable
{
    /**
     * The schema used for transformations.
     *
     * @var string[]
     */
    public $schema;

    /**
     * Returns an instance of the transformable class and loads the schema
     *
     *
     * @param string $schema
     * @return App\Maps\Transformable
     */
    public function transformWithMap($schema = ""){
        if ($schema !== ""){
            $this->schema = $schema;
        }

        return new \App\Maps\Transformable($this, $schema);
    }

    /**
     * Alias to transformWithSchema
     *
     * @param string $schema
     * @return void
     */
    public function transform($schema = ""){
        return $this->transformWithMap($schema);
    }
}

