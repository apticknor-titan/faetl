<?php

/*
|--------------------------------------------------------------------------
| Global Helpers File
|--------------------------------------------------------------------------
|
| You can use this file for any functions that should be global in your
| application. Any defined functions here can be accessed from anywhere in
| your code. Try and keep this file simple and clean.
|
*/


if (!function_exists('get_solution_name')) {

	/**
	 * Convert a solution slug into a Solution name
     * Ex:  Slug: sample
     *      Name: Sample
	 *
	 * @param string $slug
	 * @return string
	 */
	function get_solution_name($slug)
	{
		return ucfirst($slug);
	}
}

if (!function_exists('solution')) {

	/**
	 * Set or Get the current active solution
	 *
	 * @param string|null $slug
	 * @return string
	 */
	function solution($slug = "")
	{
        if (!empty($slug)){

        }
	}
}

if (!function_exists('connector')) {

	/**
	 * Return an instance of a connector
	 *
	 * @param string $slug
	 * @return mixed
	 */
	function connector($slug)
	{
        $className = sprintf("App\Connectors\%s", ucfirst($slug));
        return $className;
	}
}


if (!function_exists('context')) {

	/**
	 * Return the context instance.
	 *
	 * @param string $slug
	 * @return mixed
	 */
	function context()
	{
        return resolve('context');
	}
}


if (!function_exists('route_sub_domain')) {

	/**
	 * Dynamically get ETL subdomain base url. This is required for
	 * mapping out the project name
	 *
	 * @return string
	 */
	function route_sub_domain()
	{
		return '{project}.{environment}.' . parse_url(config('app.url'), PHP_URL_HOST);
	}
}


// if (!function_exists('connector')) {

// 	/**
// 	 * Return an instance of a connector
// 	 *
// 	 * @param string $slug
// 	 * @return mixed
// 	 */
// 	function connector($slug)
// 	{
//         $className = sprintf("App\Connectors\%s", ucfirst($slug));
//         return $className;
// 	}
// }
