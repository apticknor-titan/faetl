<?php

$firstArray = [
    'hello',
    'world',
    'fubar'
];

function testFunction($paramOne, $paramTwo, $paramThree){
    var_dump(func_get_args());
}

testFunction(...$firstArray);



die;




class TestClass {
    const HELLO = "world";

    public function hello(){
        echo "Hello " . self::HELLO;
    }
}

$class = new TestClass;
$class->hello();
die;

$object = new stdClass();
$object->hello = null;

$property1 = "hello";
$property2 = "world";

var_dump(isset($object->$property1), isset($object->$property2));

die;

// Condition 1: Body and access_token are set
$parent = new stdClass();
$child = new stdClass();

$child->access_token = "This is the token";
$parent->body = $child;

var_dump(empty($parent->body->access_token));

unset($parent);
unset($child);

// Condition 2: Body and access_token are not set
$parent = new stdClass();
$child = new stdClass();

// $child->access_token = "This is the token";
// $parent->body = $child;

var_dump(empty($parent->body->access_token));

unset($parent);
unset($child);

// Condition 3: Body and access_token are set, but access_token is an empty string ("")
$parent = new stdClass();
$child = new stdClass();

$child->access_token = "";
$parent->body = $child;

var_dump(empty($parent->body->access_token));

unset($parent);
unset($child);

