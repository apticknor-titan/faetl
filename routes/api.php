<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/describe/{project_slug?}', 'ProjectController@describe');

/**
 * Protected routes
 *
 * The routes defined below require the presence of an access token in order
 * to be consumed. The access token must also have the appropriate scopes
 * in order to consume the respective resource.
 */

Route::domain(route_sub_domain())->group(function () {

    // Users
    Route::middleware('auth:api')->post('/{solution}/', 'ProjectController@run');  ///{project_slug}
    Route::middleware('auth:api')->get('/jobs/{job_id?}', 'JobController@show');   ///{project_slug}


});
