# API Overview
**Host:** https://{client_id}.{staging|production}.irishtitan.cloud

**Authorization:** Bearer token

## Remarks
For security purposes, any malformed request (Including failed authentication) will return a 404.

# Import
POST: `/api/import`

Headers:
- Authorization: Bearer <token>
- Accept: application/json

```json
{
    "records": [
        {
            // Record data here
        }
    ]
}
```

Response:
```json
{
    "state": "success|fail",
    "reason": "description of success or failure of queueing the job, not whether the job was successful.",
    "job_id": "integer of the job ID"
}
```

# Jobs
GET: `/api/jobs/{job_id?}`
Headers:
- Authorization: Bearer <token>
- Accept: application/json

```json
{
    "job_id": "UUID",
    "status": "queued|processing|completed|error",
    "messages": [
        {
            "message_id":"UUID",
            "contents":"Output from the job"
        }
    ]
}
```

## Remarks
If `job_id` is provided, the job status is returned. If no `job_id` is provided, a list of active jobs will be displayed.

# Webhooks
Webhooks will POST a JSON body to a specific endpoint. The request will contain headers that describe the event being dispatched as well as a UUID for the event.

## Headers
`X-Event-Key`: The event key of the event that triggered the webhook.

`X-Hook-UUID`: A UUID of the request. Your implementation should record this UUID and ensure the system REJECTS and REPORTS any duplicate requests.

## Event: InventoryChanged
Headers:
- Accept: application/json
- X-Event-Key: InventoryChanged

```json
{
    "product": {
        "sku": "124521",
        "location": "main",
        "quantity": "9201"
    }
}
```

## Event: SalesOrderCreated
Headers:
- Accept: application/json
- X-Event-Key: SalesOrderCreated

```json
{
    "order": {
        /* order record */
    },
    "orderitems": [
        "orderitem": {
            /* order item record */
        }
    ]
}
```

## Event: SalesOrderChanged
Headers:
- Accept: application/json
- X-Event-Key: SalesOrderChanged

```json
{
    "order": {
        /* order record */
    },
    "orderitems": [
        "orderitem": {
            /* order item record */
        }
    ]
}
```



## Event: CustomerCreated
Headers:
- Accept: application/json
- X-Event-Key: CustomerCreated

```json
{
    "customer": {
        /* customer record */
    }
}
```

## Event: CustomerChanged
Headers:
- Accept: application/json
- X-Event-Key: CustomerChanged

```json
{
    "customer": {
        /* customer record */
    }
}
```

# Products
## Data Structure
```json
{
    "type": "product",
    "price_id": "string|optional",
    "sku":"string|unique",
    "name": "string",
    "subtitle": "string|optional",
    "description":"string|optional",
    "meta_description":"string|optional",
    "state":"boolean|required",
    "quantity":"int|required",
    "tax_class":"string|optional",
    "weight":"string|optional",
    "length":"string|optional",
    "width":"string|optional",
    "height":"string|optional",
    "manufacturer":"string|optional",
    "custom_attribute":"string|optional"
}
```

## Sample
```json
{
    "type": "product",
    "price_id": "5d56f346fdce270d817eb332",
    "sku":"H20104",
    "name": "H201 Boston Easy Couple General Purpose Hose",
    "description":"Typical Application: General purpose hose. Air and diesel fuel applications.",
    "meta_description":"Nitrile, 1 Fiber Braid, Neoprene (Black), Vinyl Nitrile (Colored Hoses), -40°F to +200°F",
    "state":"enabled",
    "quantity":"10000",
    "weight":"144",
    "length":"24",
    "width":"24",
    "height":"24",
    "manufacturer":"Limited Hose Corp",
    "inner_diameter":".25",
    "outer_diameter":".50",
    "working_pressure":"250",
    "min_burst_pressure":"1000",
    "min_bend_radius":"3.00",
    "weight_per_each":"9",
    "bundle_options": {
        "First Option": [
            {
                "sku": "24-MB01",
                "default_quantity": "1"
            },   
            {
                "sku": "24-MB02",
                "default_quantity": "1"
            }
        ],
        "Second Option": [
            {
                "sku": "MH04-XS-Green",
                "default_quantity": "1",
                "user_defined":"true"
            },
            {
                "sku": "MH04-XS-White",
                "default_quantity": "1",
                "user_defined":"true"
            }
        ]
    }
}
```

## Remarks
Fields marked "required" should be in the record payload. If it is not, the record will not be processed and an error will be reported. `price_id` must be included if a price payload is not included with the `sku` of the product in the `product_id` field of the `price` model. Custom attributes must be defined in Magento before sending them through the integration. The property in the JSON payload should be the `attribute_code` in Magento and the value should be the value exactly as it appears in Magento if using a select/multiselect field.

# Price
## Data Structure
```json
{
    "type": "price",
    "product_id": "string|required",
    "customer_id":"string|optional",
    "group_id":"string|optional",
    "amount":"float|required",
    "tier_qty": "integer|optional",
    "type": "string|options[mapp,msrp,regular,sale]",
    "start_at": "timestamp(ISO_8601)|optional",
    "end_at":"timestamp(ISO_8601)|required|depends-on:start_at"
}
```

## Sample
```json
{
    "type": "price",
    "product_id": "124521",
    "amount":"12.99",
    "type": "regular",
},
{
    "type": "price",
    "product_id": "124521",
    "amount":"9.99",
    "tier_qty": "100",
    "type": "sale",
    "start_at": "2019-08-16T19:53:28+00:00",
    "end_at":"2019-08-17T19:53:28+00:00"
}
```

## Remarks
`tier_qty` represents that quantity in the cart required in order to display the `amount` listed. If `start_at` is specified, `end_at` is required. `product_id` represents the `sku` from the `product` model provided. `product_id` takes precedence over `price_id` in the product model.

`customer_id` is either the Magento entity ID for the customer, or their email address.


# Image
## Data Structure
```json
{
    "type": "image",
    "entity_type": "string|required|options[product,customer]",
    "entity_id":"string|required",
    "data": "string|required",
    "is_thumbnail":"bool|optional"
}
```

## Sample
```json
{
    "type": "image",
    "entity_type": "product",
    "entity_id":"124521",
    "data": "https://via.placeholder.com/150",
    "is_thumbnail":"true"
}
```

## Remarks
The image data may either be a URL or a Base64 Encoded string that contains image data


# Customer
## Data Structure
```json
{
    "type": "customer",
    "customer_group": "string|required",
    "first_name":"string|required",
    "middle_name": "string|optional",
    "last_name": "string|required",
    "phone": "string|required",
    "email":"string|required",
    "gender":"string|optional",
    "dob":"string|optional",
    "state":"boolean|required",
    "password":"string|optional"
}
```

## Sample
```json
{
    "type": "customer",
    "customer_group": "retail",
    "first_name":"Bob",
    "middle_name": "C",
    "last_name": "Testuser",
    "phone": "414-215-0001",
    "email":"bob.testuser@gmail.none",
    "state":"enabled"
}
```

## Remarks
Customer index is the email property and is UNIQUE. Omit properties that are empty. If the customer already exists, fields will be updated in the Customer Record with the values passed in, except for the email. If the customer group does not exist, it will be created.



# Address
## Data Structure
```json
{
    "type": "address",
    "status": "string|required",
    "entity_type":"string|required|options[customer,order]",
    "entity_id": "string|required",
    "first_name": "string|required",
    "last_name": "string|required",
    "street1":"string|required",
    "street2":"string|required",
    "city":"string|required",
    "region":"string|required",
    "postcode":"string|required",
    "country":"string|required",
    "email":"string|required",
    "phone":"string|required"
}
```

## Sample
```json
{
    "type": "address",
    "status": "billing",
    "entity_type":"customer",
    "entity_id": "bob.testuser@gmail.none",
    "first_name": "Robert",
    "last_name": "Testuser",
    "street1":"123 Fake St",
    "street2":"Apt 101",
    "city":"Fake City",
    "region":"MN",
    "postcode":"55426",
    "country":"US",
    "email":"bob.testuser@gmail.none",
    "phone":"414-215-0001"
}
```

## Remarks
All fields are required. Including email and phone.


# SalesRule
## Data Structure
```json
{
    "type": "salesrule",
    "name": "string|required",
    "description":"string|required",
    "start_at": "timestamp(ISO_8601)|optional",
    "end_at":"timestamp(ISO_8601)|required|depends-on:start_at",
    "uses_per_customer": "int|required",
    "state":"boolean|required",
    "conditions":[
        {
            "attribute":"string|required",
            "comparator":"string|required",
            "value":"string|required"
        }
    ],
    "actions":[
        {
            "action":"string|required",
            "value":"string|required"
        }
    ],
    "targets":[
        {
            "target":"string|required",
            "value":"string|required"
        }
    ]
}
```

## Sample
```json
{
    "type": "salesrule",
    "name": "cart_blackfriday_2019",
    "description":"25% off all valves and free shipping",
    "start_at": "2019-11-29T00:00:00+0000",
    "end_at":"2019-11-30T00:00:00+0000",
    "uses_per_customer": "-1",
    "state":"true",
    "conditions":[
        {
            "attribute":"category",
            "comparator":"eq",
            "value":"valve"
        }
    ],
    "actions":[
        {
            "action":"discount_percentage",
            "value":"25.00"
        },
        {
            "action":"free_shipping",
            "value": "true"
        }
    ]
}
```

## Remarks
The available conditional statements are documented [here](https://docs.magento.com/m2/ee/user_guide/marketing/price-rules-cart.html).

If supplying `start_at` you must supply `end_at`.


# Gift Cards
## Data Structure
```json
{
    "type": "giftcard",
    "customer": "string|required",
    "code":"string|required",
    "amount": "decimal|required"
}
```

## Sample
```json
{
    "type": "giftcard",
    "customer": "bob.testuser@email.none",
    "code":"ABC-123-4567-8900",
    "amount": "500.00"
}
```

## Remarks
If `code` is a non-unique value, the `amount` specified in the payload will set the balance remaining on the gift card.

# Order
## Data Structure
```json
{
    "type": "order",
    "customer_id": "string|required",
    "order_created_at":"string|required",
    "status": "string|required",
    "customer_first_name": "string|required",
    "customer_last_name": "string|required",
    "bill_street1":"string|required",
    "bill_street2":"string|required",
    "bill_city":"string|required",
    "bill_region":"string|required",
    "bill_postcode":"string|required",
    "bill_country":"string|required",
    "bill_email":"string|required",
    "bill_phone":"string|required",
    "ship_street1":"string|required",
    "ship_street2":"string|required",
    "ship_city":"string|required",
    "ship_region":"string|required",
    "ship_postcode":"string|required",
    "ship_country":"string|required",
    "ship_email":"string|required",
    "ship_phone":"string|required",
    "payment_method":"string|required",
    "payment_data":"string|required",
    "payment_amount":"float|required",
    "shipping_method":"string|required",
    "shipping_amount":"float|required",
    "tax_amount":"float|required",
    "grand_total_amount":"floatrequired"
}
```

## Sample
```json
{

    "type": "order",
    "order_id": "1000001",
    "customer_id": "bob.testuser@gmail.none",
    "order_created_at":"2019-11-29T00:00:00+0000",
    "customer_id": "bob.testuser@gmail.none",
    "customer_first_name": "Robert",
    "customer_last_name": "Testuser",
    "bill_address_id":"123",
    "bill_street1":"123 Fake St",
    "bill_street2":"Apt 101",
    "bill_city":"Fake City",
    "bill_region":"MN",
    "bill_postcode":"55426",
    "bill_country":"US",
    "bill_email":"bob.testuser@gmail.none",
    "bill_phone":"414-215-0001",
    "ship_address_id":"123",
    "ship_street1":"123 Fake St",
    "ship_street2":"Apt 101",
    "ship_city":"Fake City",
    "ship_region":"MN",
    "ship_postcode":"55426",
    "ship_country":"US",
    "ship_email":"bob.testuser@gmail.none",
    "ship_phone":"414-215-0001",
    "payment_method":"creditcard",
    "payment_data": "<encrypted data>",
    "payment_amount":"12.99",
    "shipping_method":"ups_ground",
    "shipping_amount":"5.99",
    "tax_amount":"0.00",
    "grand_total_amount":"18.98"

}
```

## Remarks
`payment_data` will be populated during order retrieval.



# OrderItem
## Data Structure
```json
{
    "type": "orderitem",
    "product_id": "string|required",
    "order_id":"string|required",
    "name": "string|required",
    "description": "string|required",
    "meta_description": "string|required",
    "price_amount":"float|required",
    "discount_amount":"float|required",
    "tax_amount":"float|required",
    "shipping_amount":"float|required",
    "weight":"float|required",
    "length":"float|required",
    "width":"float|required",
    "height":"float|required"
}
```

## Sample
```json
{
    "type": "orderitem",
    "product_id": "124521",
    "order_id":"1000001",
    "sku":"H20104",
    "name": "H201 Boston Easy Couple General Purpose Hose",
    "description":"Product Name",
    "meta_description":"Product Notes",
    "weight":"16",
    "length":"5.5",
    "width":"1",
    "height":"8.5",
    "price_amount":"12.99",
    "discount_amount":"0.00",
    "tax_amount":"0.00",
    "shipping_amount":"5.99",
}
```

# Shipment
## Data Structure
```json
{
    "type": "shipment",
    "order_id": "string|required",
    "tracking_number":"string|required",
    "service_name": "string|required",
    "service_method":"string|required",
    "label_image": "string|optional",
    "expected_date":"string|optional"
}
```

## Sample
```json
{
    "type": "shipment",
    "order_id": "12345",
    "tracking_number":"1Z204E380338943508",
    "service_name": "UPS",
    "service_method":"Next Day AM",
    "label_image": "URL or base64 encoded image/pdf",
    "expected_date":"2019-11-29T00:00:00+0000"
}
```

## Remarks
The `label_image` field is not required. This feature is not a built-in feature for Magento 2 but is used on other platforms. The `label_image` can either be a URL to a public-facing image file (or PDF) or a base64 encoded file. 

`expected_date` is user-facing and will describe when the shipment is expected to arrive.

Shipments cannot be updated via the integration once they are created. If a shipment has to be changed after the fact, an Administrator should update it via the admin panel in Magento 2.

# Invoices

## Remarks
In Magento 2 land, invoices are records of payment for an order document. Invoices should not be created via the integration as payment is handled at the time of checkout.

# Tax Rate
## Data Structure
```json
{
    "type": "tax_rate",
    "name": "string|required",
    "postcode_is_range": "boolean|required",
    "postcode_start": "number|required",
    "postcode_end": "number|optional|depends-on:postcode_is_range",
    "region": "string|optional",
    "country": "string|required",
    "percent": "float|required"
}
```

## Sample
```json
{
    "type": "tax_rate",
    "name": "US-MN-*-Rate 1",
    "postcode_is_range": "true",
    "postcode_start": "55000",
    "postcode_end": "55999",
    "region": "MN",
    "country": "USA",
    "percent": "8.500"
}
```

## Remarks
A `tax_rate` represents a single tax rate in a given a rea and not a rate on a specific good type. 


# Tax Rule
## Data Structure
```json
{
    "type": "tax_rule",
    "name": "string|required",
    "tax_rates": "string|required",
    "customer_tax_classes": "string|required",
    "product_tax_classes": "string|required"
}
```

## Sample
```json
{
    "type": "tax_rule",
    "name": "string|required",
    "tax_rates": "US-MN-*-Rate 1, US-NJ-*-Rate 1",
    "customer_tax_classes": "Retail Customer",
    "product_tax_classes": "Taxable Goods"
}
```

## Remarks
A `tax_rule` is used to associate a rate and a class. For example, lets say that the company pivots to selling general hardware store items online. Tools are taxed at 10% and Materials are taxed at 15%. You would create a `tax_rate` for 10% and a `tax_rate` for 15%. Then create a `tax_rule` called `Tools` and a `tax_rule` called `Materials`. 

## Beer Sample
```json
{
    "type": "tax_rule",
    "name": "US Hammer Rule",
    "tax_rates": "US-MN-*-Hammer",
    "customer_tax_classes": "Retail Customer",
    "product_tax_classes": "hammer"
}
```


# Inventory
## Data Structure
```json
{
    "type": "inventory",
    "sku": "string|required",
    "location": "string|required",
    "quantity": "string|required"
}
```

## Sample
```json
{
    "type": "inventory",
    "sku": "H20104",
    "location": "100",
    "quantity": "56"
}
```

## Remarks
The `location` is the `code` of the location as listed in Magento 2. The integration does not support requesting a list of locations as the source of truth is the ERP. The locations should be added to Magento 2 manually and the `code` stored in ERP. 
