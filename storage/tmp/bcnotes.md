# BigCommerce Webhook Samples
## Failed Payment On Order
When a failed payment happens, the order is created and deleted in the customer scope and marked as incomplete in the admin scope.

## Customer Scope Events
### store/order/created
This event appears to be dispatched twice?
```
{
  "scope": "store/order/created",
  "store_id": "1000252542",
  "data": {
    "type": "order",
    "id": 106
  },
  "hash": "0acfe2cb9f93c394a2dc8959363480f8a0813c62",
  "created_at": 1568834095,
  "producer": "stores/p0ciogg8xh"
}
```

### store/order/updated
```
{
  "scope": "store/order/updated",
  "store_id": "1000252542",
  "data": {
    "type": "order",
    "id": 106
  },
  "hash": "e2d9b1feb18c9b64da1a8185ca734433ca03bc3d",
  "created_at": 1568834095,
  "producer": "stores/p0ciogg8xh"
}

When an event is triggered, a POST request is made to the endpoint with a "lightweight callback payloads" attached. The payload contains a model and an ID. According to the event type, we should then make a request to the API to get the full data model that was identified. 

For example: An order is created in BigCommerce. A store/order/created event is dispatched. We receive the event, the model "order" and the ID "108". We then make an API call to the order endpoint and get the data model. We process the event into the queue and dispatch it using the outbound connector.

Sometimes the webhook is dispatched multiple times so we should "debounce" the event. 
